package kamala.kalpana.http;

import kamala.kalpana.StateCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FListClient extends HashMap<String, Object> implements AutoCloseable
{
  private static final ExecutorService DEFAULT_EXECUTOR = Executors.newCachedThreadPool(r -> {
    Thread t = new Thread(r); // TODO: Thread Names
    t.setDaemon(true);
    return t;
  });

  private StateCache stateCache;
  private ExecutorService executorService;

  private CloseableHttpClient httpClient;
  private CookieStore cookieStore;

  public FListClient()
  {
    this(new BasicCookieStore());
  }

  public FListClient(CookieStore cookieStore)
  {
    this(new StateCache(), cookieStore, DEFAULT_EXECUTOR);
  }

  public FListClient(ExecutorService executorService)
  {
    this(new StateCache(), new BasicCookieStore(), executorService);
  }

  public FListClient(StateCache stateCache, CookieStore cookieStore)
  {
    this(stateCache, cookieStore, DEFAULT_EXECUTOR);
  }

  public FListClient(StateCache stateCache, ExecutorService executorService)
  {
    this(stateCache, new BasicCookieStore(), executorService);
  }

  public FListClient(StateCache stateCache, CookieStore cookieStore, ExecutorService executorService)
  {
    this.stateCache = stateCache;
    this.cookieStore = cookieStore;
    this.executorService = executorService;

    httpClient = HttpClients
            .custom()
            .setDefaultCookieStore(cookieStore)
            .build();
  }

  public StateCache getState()
  {
    return stateCache;
  }

  public final void executeRequest(HttpUriRequest request, ResponseHandler<Void> responseHandler)
  {
    executorService.execute(() -> {
      try
      {
        getHttpClient().execute(request, responseHandler);
        // TODO: Better error handling is going to be needed here.
      }
      catch (ClientProtocolException e)
      {
        e.printStackTrace();
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    });
  }

  public final boolean hasToken() { return this.containsKey("csrf_token") && this.get("csrf_token") instanceof BasicNameValuePair; }
  public final BasicNameValuePair getToken()
  {
    return (BasicNameValuePair) this.get("csrf_token");
  }

  public final CloseableHttpClient getHttpClient() { return httpClient; }
  public final CookieStore getCookies() { return cookieStore; }

  @Override
  public void close() throws Exception
  {
    getHttpClient().close();
  }
}
