package kamala.kalpana.http;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import kamala.kalpana.config.LotusConfiguration;
import kamala.kalpana.http.character.CharacterAPI;
import kamala.kalpana.http.login.LoginAPI;
import kamala.kalpana.jackson.LotusModule;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;

public class LotusTree
{
  public static void main(String[] args)
  {
    System.out.println("Initializing Lotus Tree Test Sequence");

    LotusConfiguration cfg = LotusConfiguration.getInstance();

    try(FListClient client = new FListClient())
    {
      CompletableFuture<Boolean> login = LoginAPI.Site.AttemptLogin(client, cfg.getAccount(), cfg.getPassword());

      CountDownLatch cdl = new CountDownLatch(1);

      login.whenComplete((siteLogin, throwable1) -> {
        System.out.println("Logged in Successfully");

        String tchar = "Zatanna";

        LoginAPI.Site.GetSession(client)
                .thenAccept(apiTicket -> {
                  System.out.println("Got API Ticket.");

                  //CharacterAPI.ViewWithAPI(client, tchar);

                  CharacterAPI.CharacterPart.GetImageData(client, apiTicket, tchar)
                          .thenAccept(imageData -> {
                            System.out.println(imageData);
                            cdl.countDown();
                          });
                });
      });

      cdl.await();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  private static Object mMutex = new Object();
  private static ObjectMapper mObjectMapper;
  public static ObjectMapper getObjectMapper()
  {
    if(mObjectMapper == null)
    {
      synchronized (mMutex)
      {
        mObjectMapper = new ObjectMapper();
        mObjectMapper.disable(com.fasterxml.jackson.databind.SerializationFeature.INDENT_OUTPUT);

        mObjectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

        mObjectMapper.registerModule(new LotusModule());
      }
    }

    return mObjectMapper;
  }
}