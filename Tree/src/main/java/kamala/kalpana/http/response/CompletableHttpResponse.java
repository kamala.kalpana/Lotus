package kamala.kalpana.http.response;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public class CompletableHttpResponse extends CompletableFuture<HttpResponse> implements ResponseHandler<Void>
{
  @Override
  public Void handleResponse(HttpResponse response) throws ClientProtocolException, IOException
  {
    this.complete(response);
    return null;
  }
}
