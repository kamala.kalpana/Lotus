package kamala.kalpana.http.friend;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.http.json.AbstractAPIData;

import java.util.List;

public class FriendList extends AbstractAPIData
{
  List<Friend> friends;
  List<PendingFriend> pending;

  @Override
  public String getType()
  {
    return "FriendList";
  }

  @JsonProperty("friends")
  public List<Friend> getFriends()
  {
    return friends;
  }
  public void setFriends(List<Friend> friends)
  {
    this.friends = friends;
  }

  @JsonProperty("pending")
  public List<PendingFriend> getPending()
  {
    return pending;
  }
  public void setPending(List<PendingFriend> pending)
  {
    this.pending = pending;
  }

  @Override
  public String toString()
  {
    return "FriendList{" +
            "friends=" + friends +
            ", pending=" + pending +
            '}';
  }

  class Friend
  {
    private int source_character_id;
    private String source_name;

    @JsonProperty("source_name")
    public String getSourceName()
    {
      return source_name;
    }
    public void setSourceName(String source_name)
    {
      this.source_name = source_name;
    }

    @JsonProperty("source_character_id")
    public int getSourceCharacterID()
    {
      return source_character_id;
    }
    public void setSourceCharacterID(int source_character_id)
    {
      this.source_character_id = source_character_id;
    }

    @Override
    public String toString()
    {
      return "Friend{" +
              "source_character_id=" + source_character_id +
              ", source_name='" + source_name + '\'' +
              '}';
    }
  }

  class PendingFriend
  {
    private int request_id;
    private int source_character_id;
    private String source_name;

    public int getRequestID()
    {
      return request_id;
    }
    public void setRequestID(int request_id)
    {
      this.request_id = request_id;
    }

    public int getSourceCharacterID()
    {
      return source_character_id;
    }
    public void setSourceCharacterID(int source_character_id)
    {
      this.source_character_id = source_character_id;
    }

    public String getSourceName()
    {
      return source_name;
    }
    public void setSourceName(String source_name)
    {
      this.source_name = source_name;
    }

    @Override
    public String toString()
    {
      return "PendingFriend{" +
              "request_id=" + request_id +
              ", source_character_id=" + source_character_id +
              ", source_name='" + source_name + '\'' +
              '}';
    }
  }
}
