package kamala.kalpana.http.friend;

import edu.umd.cs.findbugs.annotations.NonNull;
import kamala.kalpana.http.CSRFToken;
import kamala.kalpana.http.FListClient;
import kamala.kalpana.http.HTTPUtilities;
import kamala.kalpana.http.json.EmptyData;
import kamala.kalpana.http.response.CompletableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;

import java.util.concurrent.CompletableFuture;

public interface FriendAPI
{
  String GET_FRIENDS_URL = HTTPUtilities.ComposeURL("json", "getFriends.json");

  static CompletableFuture<FriendList> List(FListClient client, int characterId)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<FriendList> rv = raw_response
            .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, FriendList.class));

    HttpUriRequest req = RequestBuilder
            .post(GET_FRIENDS_URL)
            .addParameter("character_id", characterId + "")
            .build();

    client.executeRequest(req, raw_response);

    return rv;
  }

  String FRIEND_REQUEST_URL = HTTPUtilities.ComposeURL("json", "friendRequest.json");

  static CompletableFuture<Boolean> Request(FListClient client, @NonNull CSRFToken token, int source, int dest)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<Boolean> rv = raw_response
            .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, EmptyData.class))
            .thenApply(HTTPUtilities::IsDataOkay);

    HttpUriRequest req = RequestBuilder
            .post(FRIEND_REQUEST_URL)
            .addParameter(token)
            .addParameter("source_character_id", source + "")
            .addParameter("dest_character_id", dest + "")
            .build();

    client.executeRequest(req, raw_response);

    return rv;
  }

  String FRIEND_REMOVE_URL = HTTPUtilities.ComposeURL("json", "defriend.json");

  static CompletableFuture<Boolean> Remove(FListClient client, int source, int dest)
  {
    CompletableHttpResponse raw_response = new CompletableHttpResponse();

    CompletableFuture<Boolean> rv = raw_response
            .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, EmptyData.class))
            .thenApply(HTTPUtilities::IsDataOkay);

    HttpUriRequest req = RequestBuilder
            .post(FRIEND_REMOVE_URL)
            .addParameter("source_character_id", source + "")
            .addParameter("dest_character_id", dest + "")
            .build();

    client.executeRequest(req, raw_response);

    return rv;
  }
}
