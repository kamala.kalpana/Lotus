package kamala.kalpana.http;

import org.apache.http.message.BasicNameValuePair;

public class CSRFToken extends BasicNameValuePair
{
  /**
   * Default Constructor taking a name and a value. The value may be null.
   *
   * @param value The value.
   */
  public CSRFToken(String value)
  {
    super("csrf_token", value);
  }
}
