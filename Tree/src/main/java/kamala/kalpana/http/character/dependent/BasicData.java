package kamala.kalpana.http.character.dependent;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.http.json.AbstractAPIData;

import java.time.LocalDateTime;

public class BasicData extends AbstractAPIData
{
  @JsonProperty("character")
  private InternalCharacter character;

  public final String getName() { return character.name; }
  public final String getDescription() { return character.description; }
  public final LocalDateTime getCreationDate() { return character.datetime_created; }
  public final LocalDateTime getChangedDate() { return character.datetime_changed; }
  public final int getPageViews() { return character.pageviews; }

  @Override
  public String getType()
  {
    return "CharacterData:BasicData";
  }

  @Override
  public String toString()
  {
    return "BasicData{" +
            "name='" + character.name + '\'' +
            ", description='" + character.description + '\'' +
            ", datetime_created=" + character.datetime_created +
            ", datetime_changed=" + character.datetime_changed +
            ", pageviews=" + character.pageviews +
            '}';
  }

  private static class InternalCharacter
  {
    public String name;
    public String description;
    public LocalDateTime datetime_created;
    public LocalDateTime datetime_changed;
    public int pageviews;
  }
}