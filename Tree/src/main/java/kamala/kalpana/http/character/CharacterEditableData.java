package kamala.kalpana.http.character;

import kamala.kalpana.http.data.AbstractScrapingData;
import org.jsoup.nodes.Document;

public class CharacterEditableData extends AbstractScrapingData
{
  public CharacterEditableData(Document document)
  {
    super(document);
  }
}
