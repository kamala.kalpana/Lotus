package kamala.kalpana.http.note;

import kamala.kalpana.http.CSRFToken;
import kamala.kalpana.http.FListClient;
import kamala.kalpana.http.HTTPUtilities;
import kamala.kalpana.http.json.EmptyData;
import kamala.kalpana.http.note.folder.FolderChangeList;
import kamala.kalpana.http.note.folder.FolderHandle;
import kamala.kalpana.http.note.folder.FolderList;
import kamala.kalpana.http.note.notes.NoteHandle;
import kamala.kalpana.http.response.CompletableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public interface FolderAPI
  {
    /**
     * POST
     *
     * form data
     *    folder_id (target)
     *    notes[]
     *
     */
    String MOVE_NOTE_URL = HTTPUtilities.ComposeURL("json", "notes-setfolder.json");

    static CompletableFuture<FolderChangeList> MoveNote(FListClient client, FolderHandle target, NoteHandle note)
    {
      try
      {
        return MoveNote(client, NoteAPI.GetCSRFToken(client).get(), target, note);
      }
      catch (InterruptedException e)
      {
        //e.printStackTrace();
      }
      catch (ExecutionException e)
      {
        //e.printStackTrace();
      }

      return CompletableFuture.completedFuture(null);
    }

    static CompletableFuture<FolderChangeList> MoveNote(FListClient client, CSRFToken token, FolderHandle target, NoteHandle note)
    {
      return MoveNotes(client, token, target, Collections.singletonList(note));
    }

    static CompletableFuture<FolderChangeList> MoveNotes(FListClient client, FolderHandle target, List<NoteHandle> notes)
    {
      try
      {
        return MoveNotes(client, NoteAPI.GetCSRFToken(client).get(), target, notes);
      }
      catch (InterruptedException e)
      {
        //e.printStackTrace();
      }
      catch (ExecutionException e)
      {
        //e.printStackTrace();
      }

      return CompletableFuture.completedFuture(null);
    }

    static CompletableFuture<FolderChangeList> MoveNotes(FListClient client, CSRFToken token, FolderHandle target, List<NoteHandle> notes)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();

      CompletableFuture<FolderChangeList> sl = raw_response
              .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, FolderChangeList.class));

      RequestBuilder loginRequest = RequestBuilder
              .post(MOVE_NOTE_URL)
              .addParameter(token)
              .addParameter("folder_id", target.getID() + "");

      for(NoteHandle nh : notes)
        loginRequest = loginRequest.addParameter("notes[]", nh.getNoteID() + "");

      client.executeRequest(loginRequest.build(), raw_response);

      return sl;
    }

    /**
     * POST
     *
     * form data
     *    name
     *    csrf_token
     */
    String CREATE_FOLDER_URL = HTTPUtilities.ComposeURL("json", "notes-createfolder.json");

    static CompletableFuture<Integer> Create(FListClient client, String name)
    {
      try
      {
        return Create(client, NoteAPI.GetCSRFToken(client).get(), name);
      }
      catch (InterruptedException e)
      {
        // TODO: Probably keep them suppressed.
        //e.printStackTrace();
      }
      catch (ExecutionException e)
      {
        //e.printStackTrace();
      }

      return CompletableFuture.completedFuture(-1);
    }

    static CompletableFuture<Integer> Create(FListClient client, CSRFToken csrf_token, String name)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();

      CompletableFuture<Integer> sl = raw_response
              .thenApply(httpResponse -> {
                EmptyData ed = HTTPUtilities.ResponseAsType(httpResponse, EmptyData.class);

                if(!ed.hasError())
                {
                  Map<String, Object> props = ed.getUnknownProperties();
                  return (Integer) props.getOrDefault("folder_id", Integer.valueOf(-1));
                }

                return -1;
              });

      HttpUriRequest req = RequestBuilder
              .post(CREATE_FOLDER_URL)
              .addParameter("name", name)
              .addParameter(csrf_token)
              .build();

      client.executeRequest(req, raw_response);

      return sl;
    }


    /**
     * GET
     */
    String LIST_FOLDERS_URL = HTTPUtilities.ComposeURL("json", "notes-getfolders.json");

    static CompletableFuture<FolderList> List(FListClient client)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();

      CompletableFuture<FolderList> sl = raw_response
              .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, FolderList.class));

      HttpUriRequest req = RequestBuilder
              .get(LIST_FOLDERS_URL)
              .build();

      client.executeRequest(req, raw_response);

      return sl;
    }

    /**
     * POST
     *
     * form data
     *    csrf_token
     *    folder (target id)
     *    name (new name)
     */
    String RENAME_FOLDER_URL = HTTPUtilities.ComposeURL("json", "notes-renamefolder.json");

    static CompletableFuture<Boolean> Rename(FListClient client, FolderHandle folderHandle, String name)
    {
      try
      {
        return Rename(client, NoteAPI.GetCSRFToken(client).get(), folderHandle, name);
      }
      catch (InterruptedException e)
      {
        //e.printStackTrace();
      }
      catch (ExecutionException e)
      {
        //e.printStackTrace();
      }

      return CompletableFuture.completedFuture(false);
    }

    static CompletableFuture<Boolean> Rename(FListClient client, CSRFToken token, FolderHandle folderHandle, String name)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();

      CompletableFuture<Boolean> sl = raw_response
              .thenApply(httpResponse -> {
                EmptyData ed = HTTPUtilities.ResponseAsType(httpResponse, EmptyData.class);

                if(!ed.hasError())
                {
                  Map<String, Object> props = ed.getUnknownProperties();
                  return name.equals(props.get("name"));
                }

                return false;
              });

      HttpUriRequest req = RequestBuilder
              .post(RENAME_FOLDER_URL)
              .addParameter("name", name)
              .addParameter("folder", folderHandle.getID() + "")
              .addParameter(token)
              .build();

      client.executeRequest(req, raw_response);

      return sl;
    }

    /**
     * POST
     *
     * form data
     *    folder (id)
     *    csrf_token
     */
    String DELETE_FOLDER_URL = HTTPUtilities.ComposeURL("json", "notes-deletefolder.json");

    static CompletableFuture<Boolean> Delete(FListClient client, FolderHandle folderHandle)
    {
      try
      {
        return Delete(client, NoteAPI.GetCSRFToken(client).get(), folderHandle);
      }
      catch (InterruptedException e)
      {
        //e.printStackTrace();
      }
      catch (ExecutionException e)
      {
        //e.printStackTrace();
      }

      return CompletableFuture.completedFuture(false);
    }

    static CompletableFuture<Boolean> Delete(FListClient client, CSRFToken token, FolderHandle folderHandle)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();

      CompletableFuture<Boolean> sl = raw_response
              .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, EmptyData.class))
              .thenApply(HTTPUtilities::IsDataOkay);

      HttpUriRequest req = RequestBuilder
              .post(DELETE_FOLDER_URL)
              .addParameter("folder", folderHandle.getID() + "")
              .addParameter(token)
              .build();

      client.executeRequest(req, raw_response);

      return sl;
    }
  }

