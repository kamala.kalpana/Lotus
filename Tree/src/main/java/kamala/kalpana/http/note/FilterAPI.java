package kamala.kalpana.http.note;

import com.fasterxml.jackson.core.JsonProcessingException;
import kamala.kalpana.http.CSRFToken;
import kamala.kalpana.http.FListClient;
import kamala.kalpana.http.HTTPUtilities;
import kamala.kalpana.http.LotusTree;
import kamala.kalpana.http.json.EmptyData;
import kamala.kalpana.http.note.filter.FilterHandle;
import kamala.kalpana.http.note.filter.FilterList;
import kamala.kalpana.http.response.CompletableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public interface FilterAPI
  {
    String NOTES_GET_FILTERS_URL = HTTPUtilities.ComposeURL("json", "notes-getfilters.json");

    static CompletableFuture<FilterList> Get(FListClient client)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();

      CompletableFuture<FilterList> rv = raw_response
              .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, EmptyData.class))
              .thenApply(FilterList::new);

      HttpUriRequest req = RequestBuilder
              .get(NOTES_GET_FILTERS_URL)
              .build();

      client.executeRequest(req, raw_response);

      return rv;
    }

    /**
     * POST
     *
     * form data
     *    csrf_token
     *    filters (json array of holy christ kill me)
     */
    String NOTES_SET_FILTERS_URL = HTTPUtilities.ComposeURL("json", "notes-setfilters.json");

    static CompletableFuture<Boolean> Set(FListClient client, List<FilterHandle> newFilters)
    {
      try
      {
        return Set(client, NoteAPI.GetCSRFToken(client).get(), newFilters);
      }
      catch (InterruptedException e)
      {
        //e.printStackTrace();
      }
      catch (ExecutionException e)
      {
        //e.printStackTrace();
      }

      return CompletableFuture.completedFuture(false);
    }

    static CompletableFuture<Boolean> Set(FListClient client, CSRFToken token, List<FilterHandle> newFilters)
    {
      CompletableHttpResponse raw_response = new CompletableHttpResponse();

      CompletableFuture<Boolean> sl = raw_response
              .thenApply(httpResponse -> HTTPUtilities.ResponseAsType(httpResponse, EmptyData.class))
              .thenApply(HTTPUtilities::IsDataOkay);

      try
      {
        String filters = LotusTree.getObjectMapper().writeValueAsString(newFilters);

        HttpUriRequest req = RequestBuilder
                .post(NOTES_SET_FILTERS_URL)
                .addParameter("filters", filters)
                .addParameter(token)
                .build();

        client.executeRequest(req, raw_response);

        return sl;
      }
      catch (JsonProcessingException e)
      {
        return CompletableFuture.completedFuture(false);
      }
    }
  }