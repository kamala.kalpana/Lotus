package kamala.kalpana.http.bookmark;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BookmarkHandle

{
  private int character_id;
  private String name;
  private String friendlyname;

  @JsonProperty("character_id")
  public int getCharacterID()
  {
    return character_id;
  }
  public void setCharacterID(int character_id)
  {
    this.character_id = character_id;
  }

  @JsonProperty("name")
  public String getName()
  {
    return name;
  }
  public void setName(String name)
  {
    this.name = name;
  }

  @JsonProperty("friendlyname")
  public String getFriendlyName()
  {
    return friendlyname;
  }
  public void setFriendlyName(String friendlyname)
  {
    this.friendlyname = friendlyname;
  }

  @Override
  public String toString()
  {
    return "BookmarkHandle{" +
            "character_id=" + character_id +
            ", name='" + name + '\'' +
            ", friendlyname='" + friendlyname + '\'' +
            '}';
  }
}
