package kamala.kalpana.http.bookmark;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.http.json.AbstractAPIData;

import java.util.List;

public class BookmarkList extends AbstractAPIData
{
  private List<BookmarkHandle> characters;
  private int count;

  @JsonProperty("characters")
  public List<BookmarkHandle> getCharacters()
  {
    return characters;
  }
  public void setCharacters(List<BookmarkHandle> characters)
  {
    this.characters = characters;
  }

  @JsonProperty("count")
  public int getCount()
  {
    return count;
  }
  public void setCount(int count)
  {
    this.count = count;
  }

  @Override
  public String getType()
  {
    return "BookmarkList";
  }
}
