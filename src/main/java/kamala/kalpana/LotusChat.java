package kamala.kalpana;

import com.beust.jcommander.JCommander;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import kamala.kalpana.config.LotusConfiguration;
import kamala.kalpana.connection.actions.AbstractResponderAction;
import kamala.kalpana.connection.api.response.LoginTicket;
import kamala.kalpana.connection.command.AbstractCommand;
import kamala.kalpana.connection.context.FChatContext;
import kamala.kalpana.connection.packet.AbstractPacket;
import kamala.kalpana.javafx.SeamlessWindow;
import kamala.kalpana.javafx.chat.AvatarView;
import kamala.kalpana.script.FChatScriptManager;
import kamala.kalpana.script.HostedScriptContext;
import kamala.kalpana.script.ScriptPrivilegeLevel;
import org.eclipse.fx.ui.controls.tabpane.DndTabPaneFactory;

import java.util.concurrent.ThreadFactory;

public class LotusChat extends Application
{
  public static final String CNAME = "Lotus";
  public static final String CVERSION = "0.0.0-INCUBATING";

  private LotusConfiguration cfg = LotusConfiguration.getInstance();
  private static final LotusParameters parameters = new LotusParameters();

  public static final ThreadFactory DAEMON_THREAD_FACTORY = (r) ->
  {
    Thread t = new Thread(r);

    t.setDaemon(false);
    return t;
  };

  private Stage stage;

  private SeamlessWindow loginWindow;

  public static void main(String[] args)
  {
    JCommander cmdr = new JCommander(parameters);
    cmdr.setProgramName("Lotus Chat - " + CVERSION);

    cmdr.parse(args);

    LotusDatabase.InitializeDatabase();
    AbstractPacket.InitializePackets();
    AbstractCommand.InitializeCommands();
    AbstractResponderAction.InitializeActions();

    startHeadless();

    //launch(args);

    LotusDatabase.ShutdownDatabase();

    Platform.exit();
  }

  public static LotusParameters getApplicationParameters()
  {
    return parameters;
  }

  public static void startHeadless()
  {
    LoginTicket lt = LoginTicket.GetTicket("lotus_bloom", "Xc1fKI6RFBLwqMcx3DPG");

    if(lt != null)
    {
      if(lt.hasError())
      {
        // TODO: Login Failure :(
        System.out.println("Login Failed: " + lt.getError());
      }
      else
      {
        FChatScriptManager fsm = new FChatScriptManager();
        HostedScriptContext hsc = new HostedScriptContext(FChatContext.createContext(lt), "Dr Krieger", fsm.getScriptEngine(), ScriptPrivilegeLevel.Full);

        hsc.evaluateResource("/js/TestClient.js");
      }
    }
    else
    {
      System.out.println("Login Error: Was Null!");
      // TODO: Serious login error.
    }

    System.out.println("headless done starting");
  }

  @Override
  public void start(Stage primaryStage) throws Exception
  {
    loginWindow = new SeamlessWindow(new FXMLLoader(getClass().getResource("/fxml/lotus_login.fxml")), primaryStage, 260, 300, "Lotus Chat Login", 0.0, false);
    loginWindow.getStage().setX(cfg.getX());
    loginWindow.getStage().setY(cfg.getY());
    loginWindow.getStage().show();

    stage = primaryStage;
    bindProperties();

    if(false)
    {
      HBox h = new HBox();

      {
        Pane pane = DndTabPaneFactory.createDefaultDnDPane(DndTabPaneFactory.FeedbackType.MARKER, this::setupTb1);
        HBox.setHgrow(pane, Priority.ALWAYS);
        h.getChildren().add(pane);
      }

      Stage dndStage = new Stage();
      Scene sch = new Scene(h, 300, 500);
      dndStage.setScene(sch);
      dndStage.show();

    }
  }

  private void setupTb1(TabPane tb)
  {
    tb.setTabMaxHeight(80.0);
    tb.setTabMinHeight(80.0);

    tb.setTabMaxWidth(60.0);
    tb.setTabMinWidth(60.0);

    tb.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

    {
      Tab tab = new Tab("T 1.1");

      tab.setGraphic(new AvatarView("Kamala Kalpana", 55));

      Rectangle r = new Rectangle(100, 100);
      r.setFill(Color.GREEN);

      r.setOnMouseClicked(evt -> {
        Tab nt = new Tab("TTX");
        nt.setGraphic(new AvatarView("dont do it", 55));

        Rectangle rx = new Rectangle(100, 100);
        rx.setFill(Color.BLUE);

        nt.setContent(new BorderPane(rx));
        tb.getTabs().add(nt);
      });

      tab.setContent(new BorderPane(r));
      tb.getTabs().add(tab);
    }

    {
      Tab tab = new Tab("Tab 1.2");

      tab.setGraphic(new AvatarView("dont do it", 55));

      Rectangle r = new Rectangle(100, 100);
      r.setFill(Color.RED);
      tab.setContent(new BorderPane(r));
      tb.getTabs().add(tab);
    }
  }

  @Override
  public void stop() throws Exception
  {
    LotusDatabase.ShutdownDatabase();
    cfg.saveConfiguration();

    System.out.println("Stopping.");

    super.stop();

    System.exit(0);
  }

  private void bindProperties()
  {
    cfg.xProperty().bind(stage.xProperty());
    cfg.yProperty().bind(stage.yProperty());
    cfg.widthProperty().bind(loginWindow.getScene().widthProperty());
    cfg.heightProperty().bind(loginWindow.getScene().heightProperty());
  }
}
