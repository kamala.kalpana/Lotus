package kamala.kalpana.controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.layout.Pane;
import kamala.kalpana.connection.context.FChatContext;
import kamala.kalpana.javafx.SeamlessController;
import kamala.kalpana.javafx.chat.LoginAvatarGridCell;
import kamala.kalpana.javafx.components.MetroTitleBar;
import org.controlsfx.control.GridView;
import org.tbee.javafx.scene.layout.fxml.MigPane;

import java.net.URL;
import java.util.ResourceBundle;

public class LotusOverviewController extends SeamlessController
{
  private FChatContext context;

  public GridView<String> characterIcons;

  @FXML
  private MetroTitleBar titleBar;

  @FXML
  private MigPane overviewPane;

  @FXML
  private MigPane overviewInternalPane;

  private Pane dndPane;

  private ObservableList<String> characters;

  @Override
  public void initialize(URL location, ResourceBundle resources)
  {
    this.context = FChatContext.getContext();
    this.characterIcons.setCellFactory(c -> new LoginAvatarGridCell(context));
    this.characters = characterIcons.getItems();

    if (context != null)
    {
      characters.addAll(context.getTicket().getCharacters());
    }
  }
}
