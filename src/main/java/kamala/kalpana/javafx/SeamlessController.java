package kamala.kalpana.javafx;

import javafx.fxml.Initializable;

public abstract class SeamlessController implements Initializable
{
  private SeamlessWindow sWindow;

  public SeamlessController() {}
  public SeamlessController(SeamlessWindow windowReference)
  {
    this.sWindow = windowReference;
  }

  public void setReference(SeamlessWindow swnd)
  {
    sWindow = swnd;
  }

  public final SeamlessWindow getReference()
  {
    return sWindow;
  }
}
