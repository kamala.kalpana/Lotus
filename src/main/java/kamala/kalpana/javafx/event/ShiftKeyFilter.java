package kamala.kalpana.javafx.event;

import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.input.KeyEvent;

public class ShiftKeyFilter implements EventHandler<KeyEvent>
{
  private Parent node;
  private KeyEvent recodedEvent;

  public ShiftKeyFilter(Parent node)
  {
    this.node = node;
  }

  @Override
  public void handle(KeyEvent event)
  {
    if (recodedEvent != null)
    {
      recodedEvent = null;
      return;
    }

    event.getSource();

    Parent parent = node.getParent();
    if (parent != null)
    {
      switch (event.getCode())
      {
        case ENTER:
          if (event.isShiftDown())
          {
            recodedEvent = filterShift(event);
            node.fireEvent(recodedEvent);
          }
          else
          {
            Event parentEvent = event.copyFor(parent, parent);
            node.getParent().fireEvent(parentEvent);
          }
          event.consume();
          break;

        case TAB:
          if (event.isShiftDown())
          {
            recodedEvent = filterShift(event);
            node.fireEvent(recodedEvent);
          }
          else
          {
            ObservableList<Node> children = parent.getChildrenUnmodifiable();
            int idx = children.indexOf(node);
            if (idx >= 0)
            {
              for (int i = idx + 1; i < children.size(); i++)
              {
                if (children.get(i).isFocusTraversable())
                {
                  children.get(i).requestFocus();
                  break;
                }
              }
              for (int i = 0; i < idx; i++)
              {
                if (children.get(i).isFocusTraversable())
                {
                  children.get(i).requestFocus();
                  break;
                }
              }
            }
          }
          event.consume();
          break;
      }
    }
  }

  private static KeyEvent filterShift(KeyEvent evt)
  {
    return new KeyEvent(
            evt.getEventType(),
            evt.getCharacter(),
            evt.getText(),
            evt.getCode(),
            false,
            evt.isControlDown(),
            evt.isAltDown(),
            evt.isMetaDown()
    );
  }
}
