package kamala.kalpana.javafx.forward;

import javafx.application.Platform;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;

/**
 * A MapChangeListener designed to listen for events on the map that it is added to as a listener
 * and forward those changes to the target map on the JavaFX UI thread to avoid issues with displaying
 * json that can change in the background.
 *
 * @param <K>
 * @param <V>
 */
public class MapForwardingListener<K, V> implements MapChangeListener<K, V>
{
  private ObservableMap<K, V> targetMap;

  public MapForwardingListener(ObservableMap<K, V> targetMap)
  {
    this.targetMap = targetMap;
  }

  @Override
  public void onChanged(Change<? extends K, ? extends V> change)
  {
    Platform.runLater(() -> {
      K changeKey = change.getKey();

      if(change.wasAdded())
      {
        targetMap.put(changeKey, change.getValueAdded());
      }
      if(change.wasRemoved())
      {
        targetMap.put(changeKey, change.getValueRemoved());
      }
    });
  }
}