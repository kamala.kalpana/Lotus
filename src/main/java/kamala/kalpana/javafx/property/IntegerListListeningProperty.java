package kamala.kalpana.javafx.property;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.MapChangeListener;

public class IntegerListListeningProperty extends SimpleIntegerProperty implements ListChangeListener
{
  public IntegerListListeningProperty() {}

  public IntegerListListeningProperty(int initialValue)
  {
    super(initialValue);
  }

  public IntegerListListeningProperty(Object bean, String name)
  {
    super(bean, name);
  }

  public IntegerListListeningProperty(Object bean, String name, int initialValue)
  {
    super(bean, name, initialValue);
  }

  @Override
  public void onChanged(Change c)
  {
    while(c.next())
    {
      if(c.wasAdded())
        super.set(super.get() + c.getAddedSize());
      else if(c.wasRemoved())
        super.set(super.get() - c.getRemovedSize());
    }
  }
}
