package kamala.kalpana.javafx.chat;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;
import kamala.kalpana.connection.context.ChatContext;
import kamala.kalpana.connection.context.FChatContext;
import kamala.kalpana.connection.packet.client.PrivateMessageRequest;
import kamala.kalpana.connection.packet.server.PrivateMessage;
import kamala.kalpana.connection.data.CharacterReference;
import kamala.kalpana.controllers.LotusChatController;
import kamala.kalpana.javafx.components.ManagedTab;
import kamala.kalpana.javafx.event.ShiftKeyFilter;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PrivateMessageTab extends ManagedTab implements Initializable
{
  private FChatContext fChatContext;
  private ChatContext chatContext;
  private AvatarView avatar;
  private CharacterReference characterReference;

  private FXMLLoader loader;

  @FXML
  private ListView<String> conversationView;

  @FXML
  private TextArea chatInput;

  @FXML
  private Button btnSubmit;

  public PrivateMessageTab(FChatContext fChatContext, ChatContext chatContext, CharacterReference characterReference)
  {
    super(characterReference.getName());
    super.getStyleClass().add("characterTab");

    this.fChatContext = fChatContext;
    this.chatContext = chatContext;
    this.characterReference = characterReference;

    this.loader = new FXMLLoader(getClass().getResource("/fxml/tabs/lotus_conversation_tab.fxml"));
    loader.setController(this);

    try
    {
      super.setContent(loader.load());
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }

    chatContext.getSocketClient().receiverBus().register(this);
    // TODO: Remove

    avatar = new AvatarView(characterReference.getName(), LotusChatController.ICON_SIZE);
    this.setGraphic(avatar);
  }

  public CharacterReference getCharacterReference()
  {
    return characterReference;
  }

  public final void postMessage(String from, String message)
  {
    Platform.runLater(() -> {
      conversationView.getItems().add(from + " : " + message);
    });
  }

  @Override
  public void initialize(URL location, ResourceBundle resources)
  {
    chatInput.addEventFilter(KeyEvent.KEY_PRESSED, new ShiftKeyFilter(chatInput));

    btnSubmit.disableProperty().bind(chatInput.textProperty().isEqualTo(""));
    btnSubmit.setOnAction(evt -> {
      conversationView.getItems().add(chatContext.getCharacterName() + " : " + chatInput.getText());
      chatContext.getSocketClient().sendCommand(new PrivateMessageRequest(characterReference.getName(), chatInput.getText()));
      chatInput.setText("");
    });
  }

  @Subscribe
  @AllowConcurrentEvents
  public final void onMessage(PrivateMessage cm)
  {
    if(cm.getCharacter().equals(characterReference.getName()))
    {
      Platform.runLater(() -> {
        conversationView.getItems().add(cm.getCharacter() + " : " + cm.getMessage());
      });
    }
  }
}
