package kamala.kalpana.javafx.chat;

import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import kamala.kalpana.config.ApplicationConfigurator;
import kamala.kalpana.connection.FListImageHelper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class AvatarView extends ImageView
{
  private String characterName;
  private File localImageFile;

  public AvatarView(String characterName)
  {
    this(characterName, 100.0);
  }

  public AvatarView(String characterName, double size)
  {
    super();
    this.setCache(true);

    this.characterName = characterName;
    this.localImageFile = new File(ApplicationConfigurator.getImageCacheDirectory(), characterName + ".png");

    this.setFitWidth(size);
    this.setFitHeight(size);

    if(!localImageFile.exists())
    {
      Platform.runLater(() -> this.setImage(FListImageHelper.DEFAULT_CHARACTER_AVATAR));

      tryGetCurrent();
    }
    else
    {
      Platform.runLater(() ->
      {
        try
        {
          this.setImage(new Image(Files.newInputStream(localImageFile.toPath())));
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      });
    }
  }

  private void tryGetCurrent()
  {
    Runnable r = () -> {
      FListImageHelper.cacheAvatarImage(characterName);

      Platform.runLater(() ->
      {
        try
        {
          this.setImage(new Image(Files.newInputStream(localImageFile.toPath())));
        }
        catch (IOException e)
        {
          e.printStackTrace();
        }
      });
    };

    r.run();
  }
}
