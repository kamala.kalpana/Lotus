package kamala.kalpana.javafx.chat;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import kamala.kalpana.connection.actions.AbstractResponderAction;
import kamala.kalpana.connection.context.ChatContext;
import kamala.kalpana.connection.context.FChatContext;
import kamala.kalpana.connection.command.AbstractCommand;
import kamala.kalpana.connection.packet.*;
import kamala.kalpana.controllers.LotusChatController;
import kamala.kalpana.javafx.components.ManagedTab;
import kamala.kalpana.javafx.event.ShiftKeyFilter;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class ConsoleTab extends ManagedTab implements IPacketReceiver, Initializable
{
  private FChatContext fChatContext;
  private ChatContext chatContext;
  private ImageView consoleIV;

  private FXMLLoader loader;

  @FXML
  private Label lblUsers, lblPublicChannels, lblPrivateChannels;

  @FXML
  private ListView<String> consoleOutput;

  @FXML
  private TextArea consoleInput;

  @FXML
  private Button btnConsoleSubmit;

  public ConsoleTab(FChatContext fChatContext, ChatContext chatContext)
  {
    super("Console");
    super.setIsPinned(true);
    super.setCanDrag(false);

    this.fChatContext = fChatContext;
    this.chatContext = chatContext;

    this.loader = new FXMLLoader(getClass().getResource("/fxml/tabs/lotus_console_tab.fxml"));
    loader.setController(this);

    try
    {
      super.setContent(loader.load());
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }

    MenuItem pin = new MenuItem("Pin Tab");
    MenuItem closeTab = new MenuItem("Close Tab");
    ContextMenu ctxMenu = new ContextMenu(pin, closeTab);
    super.setContextMenu(ctxMenu);

    consoleIV = new ImageView(new Image(getClass().getResourceAsStream("/image/metro_icons/appbar.console.png")));

    consoleIV.setViewport(new Rectangle2D(17.0, 20.0, 42.0, 36.0));
    consoleIV.setPreserveRatio(true);
    consoleIV.setFitWidth(LotusChatController.ICON_SIZE);
    consoleIV.setCache(true);

    super.setGraphic(consoleIV);

    chatContext.getActionBus().register(this);
    chatContext.getSocketClient().receiverBus().register(this);
    chatContext.getSocketClient().senderBus().register(this);

    super.setOnClosed(evt -> {
      chatContext.getActionBus().unregister(this);
      chatContext.getSocketClient().receiverBus().unregister(this);
      chatContext.getSocketClient().senderBus().unregister(this);
    });

    // Can't close the console tab.
    super.setOnCloseRequest(evt -> evt.consume());
  }

  @Subscribe
  @AllowConcurrentEvents
  public void onAction(AbstractResponderAction action)
  {

  }

  @Subscribe
  @AllowConcurrentEvents
  public void onClientCommand(ClientPacket cc)
  {

  }

  @Override
  @AllowConcurrentEvents
  public void handleCommand(AbstractPacket cmd)
  {

  }

  @Override
  @AllowConcurrentEvents
  public void handleClientCommand(ClientPacket cmd)
  {

  }

  @Override
  @AllowConcurrentEvents
  public void handleServerCommand(ServerPacket serverPacket)
  {
    if(serverPacket.getClass().isAnnotationPresent(PacketIdentifier.class))
    {
      PacketIdentifier cid = serverPacket.getClass().getAnnotation(PacketIdentifier.class);

      Platform.runLater(() -> {
        consoleOutput.getItems().add(cid.Identifier() + ":> " + cid.Description());
      });
    }
  }

  @Override
  public void initialize(URL location, ResourceBundle resources)
  {
    lblUsers.textProperty().bind(Bindings.format("Users Online: %d", chatContext.characterCountProperty()));
    lblPublicChannels.textProperty().bind(Bindings.format("Public Channels: %d", chatContext.publicChannelCountProperty()));
    lblPrivateChannels.textProperty().bind(Bindings.format("Private Channels: %d", chatContext.privateChannelCountProperty()));

    consoleInput.addEventFilter(KeyEvent.KEY_PRESSED, new ShiftKeyFilter(consoleInput));

    btnConsoleSubmit.disableProperty().bind(
            Bindings.or(
                    consoleInput.textProperty().isEqualTo(""),
                    consoleInput.lengthProperty().lessThan(3)
            ));



    btnConsoleSubmit.setOnAction(evt -> {
      String txt = consoleInput.getText();
      consoleInput.setText("");

      if(txt.startsWith("/"))
      {
        String cmd = txt.substring(1, txt.indexOf(' ')).trim();
        List<String> parts = Arrays.asList(txt.substring(txt.indexOf(' ')).split(" "));

        if(AbstractCommand.hasCommand(cmd))
        {
          Optional<AbstractCommand> ast = AbstractCommand.getCommandHandler(cmd);

          if(ast.isPresent() && ast.get().isConsoleUsable(chatContext, parts))
          {
            ast.get().execute(chatContext, null, chatContext.getCharacterReference(), parts);
          }
        }

      }
      else
      {
        chatContext.getSocketClient().handleSendRaw(txt);
      }
    });
  }
}
