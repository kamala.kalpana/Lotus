package kamala.kalpana.database.character;

public enum Gender
{
  None,
  Male,
  Female,
  Transgender,
  Herm,
  Shemale,
  Male_Herm,
  Cunt_boy;

  public static Gender fromString(String src)
  {
    switch(src)
    {
      case "None": return None;
      case "Male": return Male;
      case "Female": return Female;
      case "Transgender": return Transgender;
      case "Herm": return Herm;
      case "Shemale":  return Shemale;
      case "Male-Herm": return Male_Herm;
      case "Cunt-boy": return Cunt_boy;
      default: return None;
    }
  }
}
