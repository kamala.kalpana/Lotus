package kamala.kalpana.database.character;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.LotusDatabase;
import kamala.kalpana.connection.scraper.data.APIRequestData;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.entity.ContentType;

import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.List;

public class ImageReferenceData extends APIRequestData
{
  private List<ImageReference> images;

  @JsonIgnore
  public static final ResponseHandler<ImageReferenceData> RESPONSE_HANDLER = response ->
  {
    StatusLine status = response.getStatusLine();
    if(status.getStatusCode() >= 300)
    {
      throw new HttpResponseException(status.getStatusCode(), status.getReasonPhrase());
    }

    HttpEntity ent = response.getEntity();
    if(ent == null)
      throw new ClientProtocolException("Response Contained No Content!");

    ContentType contentType = ContentType.getOrDefault(ent);
    Charset charset = contentType.getCharset();
    Reader reader = new InputStreamReader(ent.getContent(), charset);

    return LotusDatabase.getObjectMapper().readValue(reader, ImageReferenceData.class);
  };

  @JsonProperty("images")
  private void setImages(List<ImageReference> images) { this.images = images; }
  public List<ImageReference> getImages() { return images; }
}
