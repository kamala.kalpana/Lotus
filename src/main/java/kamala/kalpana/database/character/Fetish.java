package kamala.kalpana.database.character;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Fetish
{
  private static final Pattern SUBLIST = Pattern.compile("FList\\.Character_expandSubs\\((?<listIdentifier>\\d+)\\)");

  private boolean isCustom;
  private String name;
  private String description;
  private List<Fetish> subFetishes = null;

  private Element getSubFetishList(int listIdentifier, Element rootElement)
  {
    return rootElement.getElementById("Character_SubfetishList" + listIdentifier);
  }

  private Fetish() {}

  public Fetish(Element source, Element rootElement)
  {
    this.isCustom = source.hasClass("CustomFetish");

    if(source.hasClass("HasSubfetishes"))
    {
      Matcher mtch = SUBLIST.matcher(source.attr("onClick"));

      if(mtch.find())
      {
        Element subList = getSubFetishList(Integer.parseInt(mtch.group("listIdentifier")), rootElement);

        Elements subKinks = subList.children();

        subFetishes = subKinks.stream().map(em -> new Fetish(em, rootElement)).collect(Collectors.toList());
      }
    }

    String initialName = StringEscapeUtils.unescapeHtml4(source.ownText().trim());

    if(initialName.contains(" "))
      initialName = initialName.substring(initialName.indexOf(' '));

    this.name = initialName.trim();
    this.description = StringEscapeUtils.unescapeHtml4(source.attr("rel").trim());
  }

  @JsonProperty("name")
  public String getName() { return name; }
  private final void setName(String name)
  {
    this.name = name;
  }

  @JsonProperty("description")
  public String getDescription() { return description; }
  private final void setDescription(String description)
  {
    this.description = description;
  }

  @JsonProperty("custom")
  public boolean isCustom() { return isCustom; }
  private final void setCustom(boolean isCustom)
  {
    this.isCustom = isCustom;
  }

  @JsonProperty("subFetishes")
  public List<Fetish> getSubFetishes() { return subFetishes; }
  private final void setSubFetishes(List<Fetish> subFetishes)
  {
    this.subFetishes = subFetishes;
  }

  @JsonIgnore
  public boolean hasSubfetishes() { return subFetishes != null; }
}
