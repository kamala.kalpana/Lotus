package kamala.kalpana.config;

public class ApplicationPlatform
{
  public enum Platform
  {
    Windows,
    Mac,
    Unix,
    Solaris,
    unsupported
  }

  private static Platform m_os = null;

  public static Platform getOS()
  {
    if(m_os == null)
    {
      String os = System.getProperty("os.name").toLowerCase();

      if(os.indexOf("win") >= 0) m_os = Platform.Windows;		// Windows
      else if(os.indexOf("mac") >= 0) m_os = Platform.Mac;			// Mac
      else if(os.indexOf("nux") >= 0) m_os = Platform.Unix;			// Linux
      else if(os.indexOf("nix") >= 0) m_os = Platform.Unix;			// Unix
      else if(os.indexOf("sunos") >= 0) m_os = Platform.Solaris;		// Solaris
      else m_os = Platform.unsupported;
    }

    return m_os;
  }
}
