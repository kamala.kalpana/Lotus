package kamala.kalpana.script;

import jdk.internal.dynalink.beans.StaticClass;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import kamala.kalpana.config.ApplicationConfigurator;
import kamala.kalpana.connection.context.ChatContext;
import kamala.kalpana.connection.context.FChatContext;
import kamala.kalpana.script.shim.*;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.io.*;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class AbstractFChatScriptContext
{
  private static final List<String> ENVIRONMENT_FILES;

  static
  {
    ENVIRONMENT_FILES = new ArrayList<>();

    ENVIRONMENT_FILES.add("/js/library/nashorn_polyfill.js");

    ENVIRONMENT_FILES.add("/js/library/lodash_3.6.0.js");
    ENVIRONMENT_FILES.add("/js/library/string_3.1.1.js");

    ENVIRONMENT_FILES.add("/js/Lotus.js");
  }

  private ScriptEngine scriptEngine;
  private ScriptContext scriptContext;
  private ScriptPrivilegeLevel scriptPrivilegeLevel;
  private Bindings bindings;

  private String targetCharacter;

  private FChatContext fChatContext;
  private ChatContext chatContext;

  private DatabaseShim databaseShim;
  private AbstractShim filesystemShim;
  private FChatContextShim fChatContextShim;
  private ChatContextShim chatContextShim;
  private SocketClientShim socketClientShim;
  private OwnerShim ownerShim;

  private AtomicBoolean allowCallbacks;

  public AbstractFChatScriptContext(FChatContext fChatContext, String targetCharacter, ScriptEngine scriptEngine, ScriptContext scriptContext)
  {
    this(fChatContext, targetCharacter, scriptEngine, scriptContext, ScriptPrivilegeLevel.Default);
  }

  public AbstractFChatScriptContext(FChatContext fChatContext, String targetCharacter, ScriptEngine scriptEngine, ScriptContext scriptContext, ScriptPrivilegeLevel scriptPrivilegeLevel)
  {
    this.scriptEngine = scriptEngine;
    this.scriptContext = scriptContext;
    this.scriptPrivilegeLevel = scriptPrivilegeLevel;

    this.allowCallbacks = new AtomicBoolean(true);

    this.bindings = scriptEngine.createBindings();
    this.scriptContext.setBindings(bindings, ScriptContext.ENGINE_SCOPE);

    this.targetCharacter = targetCharacter;
    this.fChatContext = fChatContext;
    this.chatContext = fChatContext.getChatContext(targetCharacter);

    this.databaseShim = new DatabaseShim(this);
    this.fChatContextShim = new FChatContextShim(this);
    this.chatContextShim = new ChatContextShim(this);
    this.socketClientShim = new SocketClientShim(this);
    this.ownerShim = new OwnerShim(this);
  }

  /**
   * ABSOLUTELY MUST BE CALLED BY THE SUBCLASS'S CONSTRUCTOR.
   */
  protected void initializeInternalBindings()
  {
    switch(scriptPrivilegeLevel)
    {
      case Minimal:
        initializeMinimal();
        break;
      case Default:
        filesystemShim = new DefaultFilesystemShim(this);
        initializeDefault();
        break;
      case Full:
        filesystemShim = new FullFilesystemShim(this);
        initializeFull();
        break;
    }

    // Load all the packaged files into the script context.
    ENVIRONMENT_FILES.forEach(this::evaluateResource);

    initializeBindings();
  }

  private void initializeMinimal()
  {
    exposeObject("FChat", fChatContextShim);
    exposeObject("Chat", chatContextShim);
    exposeObject("Owner", ownerShim);

    exposeClass(Instant.class);
    exposeClass(LocalDateTime.class);

    exposeClass(ScriptPrivilegeLevel.class);
  }

  private void initializeDefault()
  {
    initializeMinimal();

    exposeObject("Database", databaseShim);
    exposeObject("Filesystem", filesystemShim);
    exposeObject("Socket", socketClientShim);
    exposeObject("Scraper", new FListScraperShim(this));
    exposeObject("Script", new ScriptSystemShim(this));
  }

  private void initializeFull()
  {
    initializeDefault();

  }

  protected abstract void initializeBindings();

  /**
   * Should the callbacks into JS be called? If this is false the script is probably
   * being live-edited and thus shouldn't be called into to avoid having to deal with
   * threading issues, this is backed by an atomic boolean.
   *
   * @return
   */
  public boolean allowCallbacks()
  {
    return allowCallbacks.get();
  }
  public final void setAllowCallbacks(boolean value) { allowCallbacks.set(value); }

  /**
   *
   * @return The FChatContext associated with this hosted script context.
   */
  public FChatContext getFChatContext()
  {
    return fChatContext;
  }

  public ScriptPrivilegeLevel getScriptPrivilegeLevel()
  {
    return scriptPrivilegeLevel;
  }

  /**
   *
   * @return The ChatContext associated with this hosted script context.
   */
  public ChatContext getChatContext()
  {
    return chatContext;
  }

  public ScriptObjectMirror getEnvironment()
  {
    return (ScriptObjectMirror) bindings.get("Lotus");
  }

  /**
   * Create a blank javascript object and return it as a script object mirror within this context.
   *
   * @return
   */
  public ScriptObjectMirror getBlankObject()
  {
    ScriptObjectMirror blankFunction = (ScriptObjectMirror) getEnvironment().get("createBlankObject");

    if(blankFunction != null && blankFunction.isFunction())
    {
      return (ScriptObjectMirror) blankFunction.call(null);
    }

    return null;
  }

  /**
   * Evaluates the string within this context.
   *
   * @param str
   * @return
   */
  public boolean evaluate(String str)
  {
    try
    {
      scriptEngine.eval(str, scriptContext);
      return true;
    }
    catch (ScriptException e)
    {
      // TODO: Error Handling.
      e.printStackTrace();
    }

    return false;
  }

  public ScriptObjectMirror evaluateResult(String str)
  {
    try
    {
      ScriptObjectMirror som = (ScriptObjectMirror) scriptEngine.eval(str, scriptContext);
      return som;
    }
    catch (ScriptException e)
    {
      // TODO: Error Handling.
      e.printStackTrace();
    }

    return null;
  }


  /**
   *
   * @param relativePath Path relative to the configuration/json directory, the path when normalized must reside in the
   *                     directory or a subdirectory or this will fail.
   * @return Successfully loaded the file path into the script context or not.
   */
  public boolean evaluateFile(String relativePath)
  {
    Path cfgDirectory = ApplicationConfigurator.getConfigurationDirectory();

    Path scriptFile = cfgDirectory.resolve(relativePath);

    File sf = new File(cfgDirectory.toFile(), relativePath);

    // TODO: Better filesystem sandboxing.

    if(sf.exists() && sf.canRead())
    {
      try(Reader r = new InputStreamReader(new FileInputStream(sf), "UTF-8"))
      {
        scriptEngine.eval(r, scriptContext);
        return true;
      }
      catch (FileNotFoundException | UnsupportedEncodingException e)
      {
        // TODO: ERR HANDLIN!
        e.printStackTrace();
      }
      catch (IOException e)
      {
        // TODO: ERR HANDLIN!
        e.printStackTrace();
      }
      catch (ScriptException e)
      {
        // TODO: Err handling.
        e.printStackTrace();
      }

    }

    return false;
  }

  /**
   * Evaluates an internal resource JS file within the current context, mostly just an ease-of-use
   * kind of thing then anything else, mostly saves on typing out the inputstreamreader and try/catch.
   *
   * @param path
   * @return
   */
  public boolean evaluateResource(String path)
  {
    // Fix to deal with handling UTF-8 Encoded Files.
    try(Reader r = new InputStreamReader(getClass().getResourceAsStream(path), "UTF-8"))
    {
      scriptEngine.eval(r, scriptContext);
      return true;
    }
    catch (ScriptException e)
    {
      // TODO: Error Handling.
      e.printStackTrace();
    }
    catch (UnsupportedEncodingException e)
    {
      // TODO: Error Handling
      e.printStackTrace();
    }
    catch (IOException e)
    {
      // TODO: err handling.
      e.printStackTrace();
    }

    return false;
  }

  protected Bindings getBindings()
  {
    return bindings;
  }

  /**
   * Exposes the specified object under the specified key within this script's context
   *
   * @param key Key to expose the specified object under in the JS environment.
   * @param obj Object to expose in the JS environment.
   */
  protected void exposeObject(String key, Object obj)
  {
    bindings.put(key, obj);
  }

  /**
   * Exposes the class specified
   * @param clazz
   */
  protected void exposeClass(Class clazz)
  {
    exposeClass(clazz.getSimpleName(), clazz);
  }

  protected void exposeClass(String key, Class clazz)
  {
    bindings.put(key, StaticClass.forClass(clazz));
  }
}
