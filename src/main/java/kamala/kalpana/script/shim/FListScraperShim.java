package kamala.kalpana.script.shim;

import jdk.nashorn.api.scripting.ScriptObjectMirror;
import kamala.kalpana.connection.scraper.FListScraper;
import kamala.kalpana.database.character.CharacterProfile;
import kamala.kalpana.script.AbstractFChatScriptContext;

import java.time.Duration;
import java.time.Instant;
import java.time.Period;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FListScraperShim extends AbstractShim
{
  // TODO: Bring this profile cache to a higher place so it can be shared between all instances.
  private Map<String, CharacterProfile> profileCache;

  private static ExecutorService threadPoolExecutor = Executors.newSingleThreadExecutor();
  private static Instant last = Instant.now();
  private static final Instant cooldown = Instant.ofEpochSecond(5);

  public FListScraperShim(AbstractFChatScriptContext fChatScriptContext)
  {
    super(fChatScriptContext, fChatScriptContext.getScriptPrivilegeLevel());

    this.profileCache = new HashMap<>();

    initializeBindings();
  }

  public void getProfile(String characterName, ScriptObjectMirror onReceived)
  {
    getProfile(characterName, onReceived, false);
  }

  public void getProfile(String characterName, ScriptObjectMirror onReceived, boolean refreshProfile)
  {
    threadPoolExecutor.submit(() -> {
      if (onReceived != null && onReceived.isFunction())
      {
        CharacterProfile cp;

        if (refreshProfile || !profileCache.containsKey(characterName.toLowerCase()))
        {
          Instant current = Instant.now();


          CompletableFuture<String> cf;

          Duration remaining = Duration.between(last, current);

          //if(remaining.isNegative())

          cp = FListScraper.getCharacterInformation(characterName);
          profileCache.put(characterName, cp);
        }
        else
          cp = profileCache.get(characterName.toLowerCase());

        onReceived.call(null, cp);
      }
    });
  }

  @Override
  protected void initializeBindings()
  {
    exposeObject("ProfileCache", profileCache);
  }
}
