package kamala.kalpana.script.shim;

import com.google.common.eventbus.Subscribe;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import kamala.kalpana.connection.FChatSocketClient;
import kamala.kalpana.connection.actions.OnPrivateChannelList;
import kamala.kalpana.connection.actions.OnPublicChannelList;
import kamala.kalpana.connection.context.ChannelContext;
import kamala.kalpana.connection.context.ChatContext;
import kamala.kalpana.connection.data.ChannelReference;
import kamala.kalpana.connection.data.CharacterReference;
import kamala.kalpana.connection.packet.IPacketReceiver;
import kamala.kalpana.connection.packet.client.ChannelJoinRequest;
import kamala.kalpana.connection.packet.server.PrivateMessage;
import kamala.kalpana.script.AbstractFChatScriptContext;

import java.io.Closeable;
import java.io.IOException;
import java.util.Optional;

public class ChatContextShim extends AbstractShim
        implements
          Closeable,
          IPacketReceiver,
          ChangeListener<FChatSocketClient.SocketState>,
          MapChangeListener<ChannelReference, ChannelContext>
{
  private ChatContext chatContext;
  private ObservableMap<ChannelReference, ChannelContextShim> channelContextShims;

  private ChannelChangeListener channelChangeListener;
  private CharacterChangeListener characterChangeListener;

  public ChatContextShim(AbstractFChatScriptContext fChatScriptContext)
  {
    super(fChatScriptContext, fChatScriptContext.getScriptPrivilegeLevel());

    this.chatContext = fChatScriptContext.getChatContext();
    this.channelContextShims = FXCollections.observableHashMap();

    this.getSocketClient().stateProperty().addListener(this);
    this.getSocketClient().receiverBus().register(this);
    this.getContext().getActionBus().register(this);

    this.channelChangeListener = new ChannelChangeListener();
    this.characterChangeListener = new CharacterChangeListener();

    this.getContext().getChannelContexts().addListener(channelChangeListener);
    this.getContext().getCharacters().addListener(characterChangeListener);

    initializeBindings();
  }

  @Override
  public void close() throws IOException
  {
    this.getSocketClient().stateProperty().removeListener(this);
    this.getSocketClient().receiverBus().unregister(this);
    this.getContext().getActionBus().unregister(this);

    this.getContext().getChannelContexts().removeListener(channelChangeListener);
    this.getContext().getCharacters().removeListener(characterChangeListener);
  }

  public void tryConnect()
  {
    getSocketClient().connect();
  }

  public void disconnect() { getSocketClient().close(); }

  public String getName()
  {
    return null;
  }

  public FChatSocketClient getSocketClient()
  {
    return chatContext.getSocketClient();
  }

  public ChatContext getContext()
  {
    return chatContext;
  }

  public void joinChannel(String channelName)
  {
    ObservableMap<String, ChannelReference> channels = chatContext.getChannels();

    Optional<Entry<String, ChannelReference>> oc = channels.entrySet().stream()
            .filter(e -> e.getValue().getTitle().equalsIgnoreCase(channelName)).findFirst();

    if(oc.isPresent())
    {
      chatContext.getSocketClient().sendCommand(new ChannelJoinRequest(oc.get().getValue()));
    }
  }

  @Subscribe
  public void onPublicChannelList(OnPublicChannelList opcl)
  {
    if(getScriptContext().allowCallbacks())
      executeFunction("onPublicChannelList", getContext().getChannels());
  }

  @Subscribe
  public void onPrivateChannelList(OnPrivateChannelList opcl)
  {
    if(getScriptContext().allowCallbacks())
      executeFunction("onPrivateChannelList", getContext().getChannels());
  }

  @Subscribe
  public void onPrivateMessage(PrivateMessage pm)
  {
    if(getScriptContext().allowCallbacks())
      executeFunction("onPrivateMessage", chatContext.getCharacters().get(pm.getCharacter()), pm.getMessage());
  }

  @Override
  public void changed(ObservableValue<? extends FChatSocketClient.SocketState> observable, FChatSocketClient.SocketState oldValue, FChatSocketClient.SocketState newValue)
  {
    if(getScriptContext().allowCallbacks())
      executeFunction("onConnectionStatusChange", oldValue, newValue);
  }

  @Override
  public void onChanged(Change<? extends ChannelReference, ? extends ChannelContext> change)
  {
    if(change.wasAdded())
    {
      ChannelContextShim ccs = new ChannelContextShim(this, change.getKey(),change.getValueAdded());
      channelContextShims.put(change.getKey(), ccs);

      executeFunction("onChannelJoin", change.getKey().getTitle(), ccs);
    }
    else if(change.wasRemoved())
    {
      channelContextShims.remove(change.getKey());

      executeFunction("onChannelLeave", change.getKey().getTitle());
    }
  }

  @Override
  protected void initializeBindings()
  {
    exposeObject("Channels", channelContextShims);
    exposeObject("Operators", getContext().getChatOperators());
    exposeObject("Characters", getContext().getCharacters());
  }


  private class ChannelChangeListener implements MapChangeListener<ChannelReference, ChannelContext>
  {
    @Override
    public void onChanged(Change<? extends ChannelReference, ? extends ChannelContext> change)
    {
      if(change.wasAdded())
      {
        ChannelContextShim ccs = new ChannelContextShim(ChatContextShim.this, change.getKey(),change.getValueAdded());
        channelContextShims.put(change.getKey(), ccs);

        executeFunction("onChannelJoin", change.getKey().getTitle(), ccs);
      }
      else if(change.wasRemoved())
      {
        channelContextShims.remove(change.getKey());

        executeFunction("onChannelLeave", change.getKey().getTitle());
      }
    }
  }

  private class CharacterChangeListener implements MapChangeListener<String, CharacterReference>
  {
    @Override
    public void onChanged(Change<? extends String, ? extends CharacterReference> change)
    {
      if(change.wasAdded())
      {
        executeFunction("onCharacterConnected", change.getValueAdded());
      }
      else if(change.wasRemoved())
      {
        executeFunction("onCharacterDisconnected", change.getValueRemoved());
      }
    }
  }
}
