package kamala.kalpana.script.shim;

import kamala.kalpana.LotusChat;
import kamala.kalpana.connection.api.response.LoginTicket;
import kamala.kalpana.script.AbstractFChatScriptContext;

import java.util.List;

public class FChatContextShim extends AbstractShim
{
  public FChatContextShim(AbstractFChatScriptContext fChatScriptContext)
  {
    super(fChatScriptContext, fChatScriptContext.getScriptPrivilegeLevel());

    initializeBindings();
  }

  public List<String> getAvailableCharacters()
  {
    LoginTicket lt = getScriptContext().getFChatContext().getTicket();
    return lt != null ? lt.getCharacters() : null;
  }

  public String getVersion()
  {
    return LotusChat.CNAME + " | " + LotusChat.CVERSION;
  }

  public void exit()
  {
    System.exit(0);
  }

  @Override
  protected void initializeBindings()
  {

  }
}
