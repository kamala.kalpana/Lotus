package kamala.kalpana.script.shim;

import kamala.kalpana.connection.FChatSocketClient;
import kamala.kalpana.script.AbstractFChatScriptContext;

public class SocketClientShim extends AbstractShim
{
  private FChatSocketClient fChatSocketClient;

  public SocketClientShim(AbstractFChatScriptContext fChatScriptContext)
  {
    super(fChatScriptContext, fChatScriptContext.getScriptPrivilegeLevel());

    this.fChatSocketClient = getScriptContext().getChatContext().getSocketClient();

    initializeBindings();
  }

  public void sendRaw(String rawCommand)
  {
    getSocketClient().handleSendRaw(rawCommand);
  }

  public FChatSocketClient getSocketClient()
  {
    return fChatSocketClient;
  }

  @Override
  protected void initializeBindings()
  {
    exposeObject("State", fChatSocketClient.stateProperty());
    exposeObject("Variables", fChatSocketClient.getVariables());
  }
}
