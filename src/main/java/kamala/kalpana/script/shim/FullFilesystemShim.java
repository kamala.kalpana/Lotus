package kamala.kalpana.script.shim;

import kamala.kalpana.script.AbstractFChatScriptContext;

public class FullFilesystemShim extends AbstractShim
{
  public FullFilesystemShim(AbstractFChatScriptContext fChatScriptContext)
  {
    super(fChatScriptContext, fChatScriptContext.getScriptPrivilegeLevel());

    initializeBindings();
  }

  @Override
  protected void initializeBindings()
  {

  }
}
