package kamala.kalpana.connection.api;

import kamala.kalpana.database.character.CharacterProfile;
import kamala.kalpana.database.character.FriendData;
import kamala.kalpana.database.character.GroupData;
import kamala.kalpana.database.character.ImageReferenceData;
import org.apache.commons.io.IOUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FListAPIClient
{
  public static final String REQUEST_SCHEME = "https";
  public static final String HOST = "www.f-list.net";

  private static final String GROUP_QUERY_PATH = "/json/profile-groups.json";
  private static final String IMAGE_QUERY_PATH = "/json/profile-images.json";
  private static final String FRIEND_QUERY_PATH = "/json/profile-friends.json";

  private static final String PARAM_CID = "character_id";

  private static final ResponseHandler<String> STRING_RESPONSE_HANDLER = response ->
  {
    StatusLine status = response.getStatusLine();
    if(status.getStatusCode() >= 300)
    {
      throw new HttpResponseException(status.getStatusCode(), status.getReasonPhrase());
    }

    HttpEntity ent = response.getEntity();
    if(ent == null)
      throw new ClientProtocolException("Response Contained No Content!");

    ContentType contentType = ContentType.getOrDefault(ent);
    Charset charset = contentType.getCharset();
    Reader reader = new InputStreamReader(ent.getContent(), charset);

    StringWriter sw = new StringWriter();
    IOUtils.copy(reader, sw);
    return sw.toString();
  };

  public static String ExecuteRequest(String url, Map<String, String> parameters) throws IOException
  {
    try
    {
      URI uri = new URI(url);

      List<NameValuePair> formParams = parameters.entrySet().stream().map(entry -> new BasicNameValuePair(entry.getKey(), entry.getValue())).collect(Collectors.toList());

      UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, Consts.UTF_8);

      HttpPost htPost = new HttpPost(uri);
      htPost.setEntity(entity);

      CloseableHttpClient client = HttpClients.createDefault();

      return client.execute(htPost, STRING_RESPONSE_HANDLER);
    }
    catch (URISyntaxException e)
    {
      e.printStackTrace();
    }

    return null;
  }

  public static ImageReferenceData GetImageReferences(CharacterProfile forCharacter)
  {
    try
    {
      URI uri = new URIBuilder()
              .setScheme(REQUEST_SCHEME)
              .setHost(HOST)
              .setPath(IMAGE_QUERY_PATH)
              .build();

      List<NameValuePair> formParams = new ArrayList<>();
      formParams.add(new BasicNameValuePair(PARAM_CID, forCharacter.getId() + ""));


      UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, Consts.UTF_8);

      HttpPost htPost = new HttpPost(uri);
      htPost.setEntity(entity);

      CloseableHttpClient client = HttpClients.createDefault();

      return client.execute(htPost, ImageReferenceData.RESPONSE_HANDLER);
    }
    catch (URISyntaxException e)
    {
      e.printStackTrace();
    } catch (ClientProtocolException e)
    {
      e.printStackTrace();
    } catch (IOException e)
    {
      e.printStackTrace();
    }

    return null;
  }

  public static FriendData GetFriendInformation(CharacterProfile forCharacter)
  {
    try
    {
      URI uri = new URIBuilder()
              .setScheme(REQUEST_SCHEME)
              .setHost(HOST)
              .setPath(FRIEND_QUERY_PATH)
              .build();

      List<NameValuePair> formParams = new ArrayList<>();
      formParams.add(new BasicNameValuePair(PARAM_CID, forCharacter.getId() + ""));


      UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, Consts.UTF_8);

      HttpPost htPost = new HttpPost(uri);
      htPost.setEntity(entity);

      CloseableHttpClient client = HttpClients.createDefault();

      return client.execute(htPost, FriendData.RESPONSE_HANDLER);
    }
    catch (URISyntaxException e)
    {
      e.printStackTrace();
    } catch (ClientProtocolException e)
    {
      e.printStackTrace();
    } catch (IOException e)
    {
      e.printStackTrace();
    }

    return null;
  }

  public static GroupData GetGroupInformation(CharacterProfile forCharacter)
  {
    try
    {
      URI uri = new URIBuilder()
              .setScheme(REQUEST_SCHEME)
              .setHost(HOST)
              .setPath(GROUP_QUERY_PATH)
              .build();

      List<NameValuePair> formParams = new ArrayList<>();
      formParams.add(new BasicNameValuePair(PARAM_CID, forCharacter.getId() + ""));


      UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, Consts.UTF_8);

      HttpPost htPost = new HttpPost(uri);
      htPost.setEntity(entity);

      CloseableHttpClient client = HttpClients.createDefault();

      return client.execute(htPost, GroupData.RESPONSE_HANDLER);
    }
    catch (URISyntaxException e)
    {
      e.printStackTrace();
    } catch (ClientProtocolException e)
    {
      e.printStackTrace();
    } catch (IOException e)
    {
      e.printStackTrace();
    }

    return null;
  }
}
