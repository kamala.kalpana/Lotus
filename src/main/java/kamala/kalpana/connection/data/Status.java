package kamala.kalpana.connection.data;

public enum Status
  {
    Online,
    Looking,
    Busy,
    Away,
    DND;

    public static Status fromString(String statStr)
    {
      switch(statStr)
      {
        case "Looking":
          return Looking;
        case "Busy":
          return Busy;
        case "Away":
          return Away;
        case "DND":
          return DND;
        default:
          return Online;
      }
    }
  }