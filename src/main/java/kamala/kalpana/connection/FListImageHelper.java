package kamala.kalpana.connection;

import javafx.scene.image.Image;
import kamala.kalpana.config.ApplicationConfigurator;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.*;

public class FListImageHelper
{
  private static final String AVATAR_BASE_URL = "https://static.f-list.net/images/avatar";

  public static final Image DEFAULT_CHARACTER_AVATAR = new Image(FListImageHelper.class.getResourceAsStream("/image/default_avatar.png"));

  public static void cacheAvatarImage(String characterName)
  {
    String lcc = characterName.toLowerCase();

    HttpGet getRequest = new HttpGet(AVATAR_BASE_URL + "/" + lcc.replace(" ", "%20") + ".png");

    try(CloseableHttpClient client = HttpClients.createDefault())
    {
      HttpResponse resp = client.execute(getRequest);

      File imgOut = new File(ApplicationConfigurator.getImageCacheDirectory(), lcc + ".png");

      try(InputStream input = resp.getEntity().getContent(); OutputStream output = new FileOutputStream(imgOut))
      {
        IOUtils.copy(input, output);
      }
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
}
