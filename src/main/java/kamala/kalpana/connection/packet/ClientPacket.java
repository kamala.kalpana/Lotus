package kamala.kalpana.connection.packet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import kamala.kalpana.LotusDatabase;

/*
  Base class for all commands listed

  https://wiki.f-list.net/F-Chat_Client_Commands

 */
public abstract class ClientPacket extends AbstractPacket
{
  @JsonIgnore
  public final String getData() throws JsonProcessingException
  {
    return LotusDatabase.getObjectMapper().writeValueAsString(this);
  }
}
