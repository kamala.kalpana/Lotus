package kamala.kalpana.connection.packet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.data.ChannelReference;

public abstract class AbstractChannelPacket extends ServerPacket
{
  private String channel;

  @JsonProperty("channel")
  public final String getChannel()
  {
    return channel;
  }
  private void setChannel(String newValue)
  {
    channel = newValue;
  }

  @JsonIgnore
  public final boolean isValid(ChannelReference cr)
  {
    if(cr == null) return false;

    packetLogger.trace("Checking Valid Channel: Title {} Identifier {} This Channel {}", cr.getTitle(), cr.getIdentifier(), getChannel());

    return getChannel().equals(cr.getIdentifier());
  }
}
