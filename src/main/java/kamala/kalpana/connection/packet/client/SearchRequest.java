package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.server.internal.InternalKink;

import java.util.List;

// TODO: This class is gonna need a lot of work to be usable.
@PacketIdentifier(Identifier = "FKS", Description = "Search Request.")
public class SearchRequest extends ClientPacket
{
  private List<String> characters;

  // the big kink thing here..
  private List<InternalKink> kinks;

  private List<String> genders;
  private List<String> roles;
  private List<String> orientations;
  private List<String> positions;
  private List<String> languages;

  @JsonProperty("characters")
  public final List<String> getCharacters() { return characters; }
  public void setCharacters(List<String> channels) { this.characters = channels; }

  @JsonProperty("kinks")
  public List<InternalKink> getKinks()
  {
    return kinks;
  }
  public void setKinks(List<InternalKink> kinks)
  {
    this.kinks = kinks;
  }

  @JsonProperty("genders")
  public List<String> getGenders()
  {
    return genders;
  }
  public void setGenders(List<String> genders)
  {
    this.genders = genders;
  }

  @JsonProperty("roles")
  public List<String> getRoles()
  {
    return roles;
  }
  public void setRoles(List<String> roles)
  {
    this.roles = roles;
  }

  @JsonProperty("orientations")
  public List<String> getOrientations()
  {
    return orientations;
  }
  public void setOrientations(List<String> orientations)
  {
    this.orientations = orientations;
  }

  @JsonProperty("positions")
  public List<String> getPositions()
  {
    return positions;
  }
  public void setPositions(List<String> positions)
  {
    this.positions = positions;
  }

  @JsonProperty("languages")
  public List<String> getLanguages()
  {
    return languages;
  }
  public void setLanguages(List<String> languages)
  {
    this.languages = languages;
  }
}
