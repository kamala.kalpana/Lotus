package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "RST", Description = "Sets private rooms status to closed or open.")
public class ChannelPrivacyChangeRequest extends ClientPacket
{
  private String channel;
  private String status;

  public ChannelPrivacyChangeRequest(String channel, String status)
  {
    this.channel = channel;
    this.status = status;
  }

  @JsonProperty("status")
  public final String getStatus()
  {
    return status;
  }
  public void setStatus(String newValue)
  {
    status = newValue;
  }

  @JsonProperty("channel")
  public String getChannel()
  {
    return channel;
  }
  public void setChannel(String channel)
  {
    this.channel = channel;
  }
}
