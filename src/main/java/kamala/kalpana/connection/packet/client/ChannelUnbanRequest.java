package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "CUB", Description = "Unbans a user from a channel.")
public class ChannelUnbanRequest extends ClientPacket
{
  private String channel;
  private String character;

  public ChannelUnbanRequest(String channel, String character)
  {
    this.channel = channel;
    this.character = character;
  }

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  public void setCharacter(String newValue)
  {
    character = newValue;
  }

  @JsonProperty("channel")
  public String getChannel()
  {
    return channel;
  }
  public void setChannel(String channel)
  {
    this.channel = channel;
  }
}
