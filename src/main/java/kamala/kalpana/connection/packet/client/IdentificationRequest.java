package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.LotusChat;
import kamala.kalpana.connection.api.response.LoginTicket;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "IDN")
public class IdentificationRequest extends ClientPacket
{
  private LoginTicket ticket;
  private String character;

  public IdentificationRequest(LoginTicket ticket, String character)
  {
    this.ticket = ticket;
    this.character = character;
  }

  @JsonProperty("method")
  public final String getMethod()
  {
    return "ticket";
  }

  @JsonProperty("account")
  public final String getAccount()
  {
    return ticket.getAccountName();
  }

  @JsonProperty("ticket")
  public final String getTicket()
  {
    return ticket.getTicket();
  }

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  private void setCharacter(String newValue)
  {
    character = newValue;
  }

  @JsonProperty("cname")
  public final String getName()
  {
    return LotusChat.CNAME;
  }

  @JsonProperty("cversion")
  public final String getVersion()
  {
    return LotusChat.CVERSION;
  }
}
