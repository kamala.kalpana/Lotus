package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.AbstractChannelPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "CBU", Description = "Bans the specified user from the specified channel.")
public class ChannelBan extends AbstractChannelPacket
{
  private String operator;
  private String character;

  @JsonProperty("operator")
  public final String getOperator()
  {
    return operator;
  }
  private void setOperator(String operator)
  {
    this.operator = operator;
  }

  @JsonProperty("name")
  public final String getCharacter()
  {
    return character;
  }
  private void setCharacter(String character)
  {
    this.character = character;
  }

  @Override
  public String toString()
  {
    return "CBU: Channel: " + getChannel() + " Operator: " + getOperator() + " Name: " + getCharacter();
  }
}
