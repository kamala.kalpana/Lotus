package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.AbstractChannelPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;
import kamala.kalpana.connection.packet.server.internal.InternalUser;

@PacketIdentifier(Identifier = "JCH", Description = "Notifies that a character has joined the specified channel, also can be your own character..")
public class CharacterJoinChannel extends AbstractChannelPacket
{
  private String title;
  private InternalUser character;

  @JsonProperty("title")
  public String getTitle()
  {
    return title;
  }
  private void setTitle(String title)
  {
    this.title = title;
  }

  @JsonProperty("character")
  public InternalUser getCharacter()
  {
    return character;
  }
  private void setCharacter(InternalUser character)
  {
    this.character = character;
  }

  @Override
  public String toString()
  {
    return "JCH: Channel: " + getChannel() + " Title: " + getTitle() + " Character: " + getCharacter().getIdentity();
  }
}
