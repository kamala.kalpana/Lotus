package kamala.kalpana.connection.packet.server.internal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InternalUser
{
  private String identity;

  @JsonProperty("identity")
  public String getIdentity()
  {
    return identity;
  }
  private void setIdentity(String identity)
  {
    this.identity = identity;
  }
}
