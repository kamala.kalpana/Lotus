package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.AbstractChannelPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "SYS", Description = "System message from the server, can be mixed in with other packet responses.")
public class SystemMessage extends AbstractChannelPacket
{
  private String message;

  @JsonProperty
  public String getMessage()
  {
    return message;
  }
  private void setMessage(String message)
  {
    this.message = message;
  }

  @Override
  public String toString()
  {
    return "SYS: Channel: " + getChannel() + " Message: " + getMessage();
  }
}
