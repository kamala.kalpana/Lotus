package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "STA", Description = "User status change.")
public class UserStatusChange extends ServerPacket
{
  private String status;
  private String character;
  private String statusmsg;

  @JsonProperty("status")
  public String getStatus()
  {
    return status;
  }
  public void setStatus(String status)
  {
    this.status = status;
  }

  @JsonProperty("character")
  public String getCharacter()
  {
    return character;
  }
  public void setCharacter(String character)
  {
    this.character = character;
  }

  @JsonProperty("statusmsg")
  public String getStatusmsg()
  {
    return statusmsg;
  }
  public void setStatusmsg(String statusmsg)
  {
    this.statusmsg = statusmsg;
  }

  @Override
  public String toString()
  {
    return "STA: Character: " + getCharacter() + " Status: " + getStatus() + " StatusMessage: " + getStatusmsg();
  }
}
