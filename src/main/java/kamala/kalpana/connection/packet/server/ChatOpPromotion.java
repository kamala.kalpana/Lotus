package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "AOP", Description = "Notifies that a character has been promoted to chat operator.")
public class ChatOpPromotion extends ServerPacket
{
  private String character;

  @JsonProperty("character")
  public final String getCharacter()
  {
    return character;
  }
  private void setCharacter(String newValue)
  {
    character = newValue;
  }

  @Override
  public String toString()
  {
    return "AOP: Character: " + getCharacter();
  }
}
