package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "PRI", Description = "Private message received from another user.")
public class PrivateMessage extends ServerPacket
{
  private String character;
  private String message;

  @JsonProperty("character")
  public String getCharacter()
  {
    return character;
  }
  private void setCharacter(String character)
  {
    this.character = character;
  }

  @JsonProperty("message")
  public String getMessage()
  {
    return message;
  }
  private void setMessage(String message)
  {
    this.message = message;
  }

  @Override
  public String toString()
  {
    return "PRI: Character: " + getCharacter() + " Message: " + getMessage();
  }
}
