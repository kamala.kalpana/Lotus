package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "NLN", Description = "User connected.")
public class UserConnected extends ServerPacket
{
  private String identity;
  private String gender;
  private String status;

  @JsonProperty("identity")
  public String getIdentity()
  {
    return identity;
  }
  private void setIdentity(String identity)
  {
    this.identity = identity;
  }

  @JsonProperty("gender")
  public String getGender()
  {
    return gender;
  }
  private void setGender(String gender)
  {
    this.gender = gender;
  }

  @JsonProperty("status")
  public String getStatus()
  {
    return status;
  }
  private void setStatus(String status)
  {
    this.status = status;
  }

  @Override
  public String toString()
  {
    return "NLN: Identity: " + getIdentity() + " Gender: " + getGender() + " Status: " + getStatus();
  }
}
