package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "HLO", Description = "Server Hello packet.")
public class ServerHello extends ServerPacket
{
  private String message;

  @JsonProperty("message")
  public final String getMessage()
  {
    return message;
  }
  private void setMessage(String newValue)
  {
    message = newValue;
  }

  @Override
  public String toString()
  {
    return "HLO: Message: " + getMessage();
  }
}
