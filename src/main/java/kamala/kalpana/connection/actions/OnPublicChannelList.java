package kamala.kalpana.connection.actions;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import kamala.kalpana.connection.packet.ActionIdentifier;
import kamala.kalpana.connection.packet.server.ChannelListPublic;
import kamala.kalpana.connection.data.ChannelReference;

import java.util.List;
import java.util.stream.Collectors;

@ActionIdentifier(OnCommand = "CHA")
public class OnPublicChannelList extends AbstractResponderAction
{
  private ChannelListPublic response;
  private List<ChannelReference> channels;

  @Subscribe
  @AllowConcurrentEvents
  public void onPublicChannelList(ChannelListPublic clp)
  {
    this.response = clp;
    this.channels = clp.getChannels().stream()
            .map(ipc -> new ChannelReference(getContext(), ipc)).collect(Collectors.toList());

    getContext().onAction(this);
    unregisterListener();
  }

  public final ChannelListPublic getResponse()
  {
    return response;
  }

  public final List<ChannelReference> getChannels()
  {
    return channels;
  }
}
