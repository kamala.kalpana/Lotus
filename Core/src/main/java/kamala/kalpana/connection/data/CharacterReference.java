package kamala.kalpana.connection.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javafx.beans.property.*;
import kamala.kalpana.connection.context.ChannelContext;
import kamala.kalpana.connection.context.ChatContext;
import kamala.kalpana.connection.packet.client.PrivateMessageRequest;
import kamala.kalpana.database.character.Gender;

import java.util.List;
import java.util.Objects;

public class CharacterReference
{
  private ChatContext chatContext;

  private String name;
  private ObjectProperty<Gender> gender;
  private ObjectProperty<Status> status;
  private StringProperty statusMessage;
  private BooleanProperty isOperator;

  private CharacterReference()
  {
    this.isOperator = new SimpleBooleanProperty(this, "isOperator", false);
    this.chatContext = null;
  }

  public CharacterReference(ChatContext chatContext, List<String> parts)
  {
    this();

    this.chatContext = chatContext;

    if(parts.size() == 4)
    {
      this.name = parts.get(0);
      this.gender = new SimpleObjectProperty<>(this, "gender", Gender.fromString(parts.get(1)));
      this.status = new SimpleObjectProperty<>(this, "status", Status.fromString(parts.get(2)));
      this.statusMessage = new SimpleStringProperty(this, "statusMessage", parts.get(3));
    }
  }

  // Assumes empty status message.
  public CharacterReference(ChatContext chatContext, String name, String gender, String status)
  {
    this();

    this.chatContext = chatContext;

    this.name = name;
    this.gender = new SimpleObjectProperty<>(this, "gender", Gender.fromString(gender));
    this.status = new SimpleObjectProperty<>(this, "status", Status.fromString(status));
    this.statusMessage = new SimpleStringProperty(this, "statusMessage", "");
  }

  public String getName()
  {
    return name;
  }
  private void setName(String name)
  {
    this.name = name;
  }

  public final ObjectProperty<Gender> genderProperty() { return gender; }
  public Gender getGender()
  {
    return genderProperty().get();
  }
  private void setGender(String gender)
  {
    genderProperty().set(Gender.fromString(gender));
  }

  public final ObjectProperty<Status> statusProperty()
  {
    return status;
  }
  public Status getStatus()
  {
    return status.get();
  }
  public void setStatus(Status status)
  {
    this.status.set(status);
  }

  public final StringProperty statusMessageProperty()
  {
    return statusMessage;
  }
  public String getStatusMessage()
  {
    return statusMessage.get();
  }
  public void setStatusMessage(String statusMessage)
  {
    this.statusMessage.set(statusMessage);
  }

  public final BooleanProperty isOperatorProperty()
  {
    return isOperator;
  }
  public final boolean isOperator()
  {
    return isOperatorProperty().get();
  }
  public final void setOperator(boolean isOp)
  {
    isOperatorProperty().set(isOp);
  }

  @JsonIgnore
  public final boolean isChannelModerator(ChannelContext cc)
  {
    if(cc == null) return false;

    return cc.getModerators().stream().anyMatch(x -> x.equalsIgnoreCase(getName())) || cc.getOwner().equalsIgnoreCase(getName());
  }

  @JsonIgnore
  public final boolean isChannelOwner(ChannelContext cc)
  {
    if(cc == null) return false;

    return cc.getOwner().equalsIgnoreCase(getName());
  }

  public void sendMessage(String message)
  {
    chatContext.getSocketClient().sendCommand(new PrivateMessageRequest(this.getName(), message));
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (o instanceof CharacterReference)
    {
      CharacterReference that = (CharacterReference) o;
      return Objects.equals(getName().toLowerCase(), that.getName().toLowerCase());
    }
    else if(o instanceof String)
    {
      return Objects.equals(getName().toLowerCase(), ((String) o).toLowerCase());
    }

    return false;
  }
}
