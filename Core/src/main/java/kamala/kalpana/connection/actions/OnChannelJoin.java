package kamala.kalpana.connection.actions;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import kamala.kalpana.connection.context.ChannelContext;
import kamala.kalpana.connection.data.ChannelReference;
import kamala.kalpana.connection.packet.ActionIdentifier;
import kamala.kalpana.connection.packet.server.ChannelDescriptionUpdate;
import kamala.kalpana.connection.packet.server.ChannelOperatorList;
import kamala.kalpana.connection.packet.server.CharacterJoinChannel;
import kamala.kalpana.connection.packet.server.InitialChannelData;

@ActionIdentifier(OnCommand = "JCH")
public class OnChannelJoin extends AbstractResponderAction
{
  private ChannelContext channelContext;
  private ChannelReference channelReference;

  private CharacterJoinChannel channelJoin;
  private ChannelOperatorList channelOperators;
  private InitialChannelData initialChannelData;
  private ChannelDescriptionUpdate channelDescriptionUpdate;

  @Subscribe
  @AllowConcurrentEvents
  public void onIdentificationResponse(CharacterJoinChannel idn)
  {
    this.channelReference = getContext().getChannels().get(idn.getChannel());
    this.channelJoin = idn;
  }

  @Subscribe
  @AllowConcurrentEvents
  public void onChannelModerators(ChannelOperatorList col)
  {
    if(col.isValid(channelReference))
    {
      this.channelOperators = col;
    }
  }

  @Subscribe
  @AllowConcurrentEvents
  public void onChannelData(InitialChannelData icd)
  {
    if(icd.isValid(channelReference))
    {
      this.initialChannelData = icd;
    }
  }

  @Subscribe
  @AllowConcurrentEvents
  public void onChannelDescription(ChannelDescriptionUpdate cdu)
  {
    if(cdu.isValid(channelReference))
    {
      this.channelDescriptionUpdate = cdu;

      unregisterListener();

      this.channelContext = new ChannelContext(getContext(), channelReference);
      channelContext.onChannelDescriptionUpdate(cdu);
      channelContext.onChannelData(initialChannelData);
      channelContext.onChannelOperatorList(channelOperators);

      getContext().onAction(this);
    }
  }


  public final CharacterJoinChannel getChannelJoin()
  {
    return channelJoin;
  }

  public final String getCharacter()
  {
    return getChannelJoin().getCharacter().getIdentity();
  }

  public final String getChannelID()
  {
    return getChannelJoin().getChannel();
  }

  public final ChannelContext getChannelContext()
  {
    return channelContext;
  }
}
