package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "CRC", Description = "Creates an official channel.")
public class CreateOfficialChannelRequest extends ClientPacket
{
  private String channel;

  public CreateOfficialChannelRequest(String channel)
  {
    this.channel = channel;
  }

  @JsonProperty("channel")
  public final String getChannel()
  {
    return channel;
  }
  private void setChannel(String channel)
  {
    this.channel = channel;
  }
}
