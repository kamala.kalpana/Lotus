package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

import java.time.Instant;

@PacketIdentifier(Identifier = "UPT", Description = "Informs the client of the server's self-tracked online time, and a few other bits of information.")
public class UptimeNotification extends ServerPacket
{
  private Instant time;
  private Instant startTime;
  private String startString;
  private int accepted;
  private int channels;
  private int users;
  private int maxusers;

  @JsonProperty("time")
  public Instant getTime()
  {
    return time;
  }
  private void setTime(long time)
  {
    this.time = Instant.ofEpochSecond(time);
  }

  @JsonProperty("starttime")
  public Instant getStartTime()
  {
    return startTime;
  }
  private void setStartTime(long startTime)
  {
    this.startTime = Instant.ofEpochSecond(startTime);
  }

  @JsonProperty("startstring")
  public String getStartString()
  {
    return startString;
  }
  private void setStartString(String startstring)
  {
    this.startString = startstring;
  }

  @JsonProperty("accepted")
  public int getAccepted()
  {
    return accepted;
  }
  private void setAccepted(int accepted)
  {
    this.accepted = accepted;
  }

  @JsonProperty("channels")
  public int getChannels()
  {
    return channels;
  }
  private void setChannels(int channels)
  {
    this.channels = channels;
  }

  @JsonProperty("users")
  public int getUsers()
  {
    return users;
  }
  private void setUsers(int users)
  {
    this.users = users;
  }

  @JsonProperty("maxusers")
  public int getMaxusers()
  {
    return maxusers;
  }
  private void setMaxusers(int maxusers)
  {
    this.maxusers = maxusers;
  }

  @Override
  public String toString()
  {
    return "UPT: Not Implemented";
  }
}
