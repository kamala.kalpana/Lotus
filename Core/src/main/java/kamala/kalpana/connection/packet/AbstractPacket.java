package kamala.kalpana.connection.packet;

import kamala.kalpana.LotusCore;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public abstract class AbstractPacket
{
  protected static final Logger packetLogger = LoggerFactory.getLogger(AbstractPacket.class);

  private static Reflections reflector =
          new Reflections(new ConfigurationBuilder()
                  .setUrls(ClasspathHelper.forPackage("kamala.kalpana.connection.packet.server"))
                  .setScanners(
                          new SubTypesScanner(),
                          new TypeAnnotationsScanner())
          );
  private static Map<String, Class<? extends ServerPacket>> serverPacketCache = new HashMap<>();

  public static void InitializePackets()
  {
    Set<Class<? extends ServerPacket>> cmds = reflector.getSubTypesOf(ServerPacket.class);

    cmds.stream()
            .filter(clz -> clz.isAnnotationPresent(PacketIdentifier.class))
            .forEach((clz) -> {
              PacketIdentifier cid = getIdentifier(clz);

              serverPacketCache.put(cid.Identifier(), clz);
            });

    packetLogger.info("Server Packets Cached: " + serverPacketCache.size());
  }

  public static PacketIdentifier getIdentifier(Class<? extends AbstractPacket> cmd)
  {
    if (cmd.isAnnotationPresent(PacketIdentifier.class))
      return cmd.getAnnotation(PacketIdentifier.class);
    else return null;
  }

  /**
   * Parse a ServerCommand from the specified packet string, including generating the
   * body of the packet into the associated class by parsing the JSON.
   *
   * @param commandString Command string sent over the socket, always has the 3 digit
   *                      identifier in place first, and optionally has a JSON object body.
   * @return Parsed server packet.
   */
  public static ServerPacket parsePacket(String commandString)
  {
    String idn = commandString.substring(0, 3);
    String data = null;

    if (commandString.length() > 3)
      data = commandString.substring(4);

    ServerPacket cmd = null;

    if (serverPacketCache.containsKey(idn))
    {
      Class<? extends ServerPacket> cClass = serverPacketCache.get(idn);

      if (data != null)
      {
        try
        {
          cmd = LotusCore.getObjectMapper().readValue(data, cClass);

          //packetLogger.trace("Created Packet from JSON Reflection: {}", cmd.getClass().getName());
        }
        catch (IOException e)
        {
          e.printStackTrace();
          /* TODO: Log Exception. */
        }
      }
      else
      {
        try
        {
          cmd = cClass.newInstance();

          //packetLogger.trace("Created Packet from Reflected Constructor: {}", cmd.getClass().getName());
        }
        catch (InstantiationException e) { e.printStackTrace(); }
        catch (IllegalAccessException e) { e.printStackTrace(); }
      }
    }

    if (cmd == null)
    {
      cmd = new RawPacket(commandString);

      packetLogger.trace("Created Raw Packet: {}", cmd.getClass().getName());
    }

    return cmd;
  }
}

