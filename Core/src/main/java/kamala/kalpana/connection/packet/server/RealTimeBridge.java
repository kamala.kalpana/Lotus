package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "RTB", Description = "Real-Time Bridge indicating that a user received a note or message..")
public class RealTimeBridge extends ServerPacket
{
  private String type;
  private String character;

  @JsonProperty("type")
  public String getType()
  {
    return type;
  }
  private void setType(String type)
  {
    this.type = type;
  }

  @JsonProperty("character")
  public String getCharacter()
  {
    return character;
  }
  private void setCharacter(String character)
  {
    this.character = character;
  }

  @Override
  public String toString()
  {
    return "RTB: Type: " + getType() + " Character: " + getCharacter();
  }
}
