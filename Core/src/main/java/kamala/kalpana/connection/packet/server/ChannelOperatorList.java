package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.AbstractChannelPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

import java.util.Collections;
import java.util.List;

@PacketIdentifier(Identifier = "COL", Description = "List of operators in the specified channel.")
public class ChannelOperatorList extends AbstractChannelPacket
{
  // Entry 0 is always the owner/operator
  private List<String> ops;

  @JsonProperty("oplist")
  private final List<String> getOperators() { return ops; }
  private void setOperators(List<String> ops) { this.ops = ops; }

  @JsonIgnore
  public final String getOwner()
  {
    return ops.get(0);
  }

  @JsonIgnore
  public final List<String> getModerators()
  {
    if(getOperators().size() > 1)
    {
      return getOperators().subList(1, getOperators().size() - 1);
    }
    else
      return Collections.emptyList();
  }

  @JsonIgnore
  public final boolean hasOwner()
  {
    return getOperators().size() >= 1 && !ops.get(0).isEmpty();
  }

  @Override
  public String toString()
  {
    return "COL: Owner: " + getOwner() + " Moderators: " + getModerators().size();
  }
}
