package kamala.kalpana.connection.packet.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.ClientPacket;
import kamala.kalpana.connection.packet.PacketIdentifier;

@PacketIdentifier(Identifier = "RMO", Description = "Changes room mode to the specified type.")
public class ChannelModeChangeRequest extends ClientPacket
{
  private String channel;
  private String mode;

  public ChannelModeChangeRequest(String channel, String mode)
  {
    this.channel = channel;
    this.mode = mode;
  }

  @JsonProperty("mode")
  public final String getMode()
  {
    return mode;
  }
  public void setMode(String newValue)
  {
    mode = newValue;
  }

  @JsonProperty("channel")
  public String getChannel()
  {
    return channel;
  }
  public void setChannel(String channel)
  {
    this.channel = channel;
  }
}
