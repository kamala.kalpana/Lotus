package kamala.kalpana.connection.packet.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.connection.packet.PacketIdentifier;
import kamala.kalpana.connection.packet.ServerPacket;

@PacketIdentifier(Identifier = "ERR", Description = "An error has occurred.")
public class Error extends ServerPacket
{
  private int number;
  private String message;

  @JsonProperty("number")
  public final int getNumber() { return number; }
  private void setNumber(int ops) { this.number = ops; }

  @JsonProperty("message")
  public final String getMessage()
  {
    return message;
  }
  private void setMessage(String channel)
  {
    this.message = channel;
  }

  @Override
  public String toString()
  {
    return "ERR: Number: " + getNumber() + " Message: " + getMessage();
  }
}
