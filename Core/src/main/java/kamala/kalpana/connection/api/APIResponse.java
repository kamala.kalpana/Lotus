package kamala.kalpana.connection.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public abstract class APIResponse
{

  public APIResponse()
  {
    error = new SimpleStringProperty("");
  }
  public abstract String getType();


  private StringProperty error;


  @JsonProperty("error")
  public String getError() { return error.get(); }
  public void setError(String newX) { error.set(newX); }


  @JsonIgnore
  public boolean hasError()
  {
    return !error.get().isEmpty();
  }
}
