package kamala.kalpana.connection.api.response;

public class FriendEntry
{
  public String source_name;
  public String dest_name;

  public final String getMyCharacter()
  {
    return dest_name;
  }

  public final String getTheirCharacter() { return source_name; }
}
