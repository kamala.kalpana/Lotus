package kamala.kalpana;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class StateCache
{
  private Map<String, Object> stateMap = new ConcurrentHashMap<>();

  /**
   * Tries to retrieve the specified value and cast it to the target type from the global
   * object state.
   *
   * @param key Key to search by.
   * @param <T> Type to cast and return.
   * @return The desired object or null if it wasn't found or wasn't of the type requested.
   */
  public <T> T getValue(String key)
  {
    return stateMap.containsKey(key) ? (T) stateMap.get(key) : null;
  }

  public void setValue(StateObject so)
  {
    stateMap.put(so.getIdentifier(), so);
  }

  public void setValue(String key, Object value)
  {
    stateMap.put(key, value);
  }

  public boolean hasValue(String key)
  {
    return stateMap.containsKey(key);
  }

  public void clearValue(String key)
  {
    stateMap.remove(key);
  }

  public interface StateObject
  {
    String getIdentifier();
  }
}
