package kamala.kalpana.database.character;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.LotusCore;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.entity.ContentType;

import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.List;

public class FriendData// extends APIRequestData
{
  private List<String> friends;

  public static final ResponseHandler<FriendData> RESPONSE_HANDLER = response ->
  {
    StatusLine status = response.getStatusLine();
    if(status.getStatusCode() >= 300)
    {
      throw new HttpResponseException(status.getStatusCode(), status.getReasonPhrase());
    }

    HttpEntity ent = response.getEntity();
    if(ent == null)
      throw new ClientProtocolException("Response Contained No Content!");

    ContentType contentType = ContentType.getOrDefault(ent);
    Charset charset = contentType.getCharset();
    Reader reader = new InputStreamReader(ent.getContent(), charset);

    return LotusCore.getObjectMapper().readValue(reader, FriendData.class);
  };

  @JsonProperty("friends")
  public void setFriends(List<String> grps)
  {
    friends = grps;
  }
  public List<String> getFriends()
  {
    return friends;
  }
}
