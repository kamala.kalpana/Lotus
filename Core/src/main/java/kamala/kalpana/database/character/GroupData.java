package kamala.kalpana.database.character;

import com.fasterxml.jackson.annotation.JsonProperty;
import kamala.kalpana.LotusCore;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.entity.ContentType;

import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.List;

public class GroupData// extends APIRequestData
{
  private List<String> groups;

  public static final ResponseHandler<GroupData> RESPONSE_HANDLER = response ->
  {
    StatusLine status = response.getStatusLine();
    if(status.getStatusCode() >= 300)
    {
      throw new HttpResponseException(status.getStatusCode(), status.getReasonPhrase());
    }

    HttpEntity ent = response.getEntity();
    if(ent == null)
      throw new ClientProtocolException("Response Contained No Content!");

    ContentType contentType = ContentType.getOrDefault(ent);
    Charset charset = contentType.getCharset();
    Reader reader = new InputStreamReader(ent.getContent(), charset);

    return LotusCore.getObjectMapper().readValue(reader, GroupData.class);
  };

  @JsonProperty("groups")
  public void setGroups(List<String> grps)
  {
    groups = grps;
  }
  public List<String> getGroups()
  {
    return groups;
  }
}
