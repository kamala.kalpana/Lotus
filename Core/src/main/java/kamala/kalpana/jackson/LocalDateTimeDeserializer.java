package kamala.kalpana.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime>
{
  private static final Pattern YEAR = Pattern.compile("((?<years>[0-9]+)y)");
  private static final Pattern MONTHS = Pattern.compile("((?<months>[0-9]+)mo)");
  private static final Pattern DAYS = Pattern.compile("((?<days>[0-9]+)d)");
  private static final Pattern HOURS = Pattern.compile("((?<hours>[0-9]+)h)");
  private static final Pattern MINUTES = Pattern.compile("((?<minutes>[0-9]+)m)");
  private static final Pattern SECONDS = Pattern.compile("((?<seconds>[0-9]+)s)");

  public static LocalDateTime fromDelimited(String timeAgo)
  {
    LocalDateTime date = LocalDateTime.now();

    String dateText = new String(timeAgo);

    Matcher mtch = YEAR.matcher(dateText);
    if(mtch.find())
    {
      date = date.minusYears(Integer.parseInt(mtch.group("years")));
      dateText = dateText.replace(mtch.group(), "");
    }

    mtch = MONTHS.matcher(dateText);
    if(mtch.find())
    {
      date = date.minusMonths(Integer.parseInt(mtch.group("months")));
      dateText = dateText.replace(mtch.group(), "");
    }

    mtch = DAYS.matcher(dateText);
    if(mtch.find())
    {
      date = date.minusDays(Integer.parseInt(mtch.group("days")));
      dateText = dateText.replace(mtch.group(), "");
    }

    mtch = HOURS.matcher(dateText);
    if(mtch.find())
    {
      date = date.minusHours(Integer.parseInt(mtch.group("hours")));
      dateText = dateText.replace(mtch.group(), "");
    }

    mtch = MINUTES.matcher(dateText);
    if(mtch.find())
    {
      date = date.minusMinutes(Integer.parseInt(mtch.group("minutes")));
      dateText = dateText.replace(mtch.group(), "");
    }

    mtch = SECONDS.matcher(dateText);
    if(mtch.find())
    {
      date = date.minusSeconds(Integer.parseInt(mtch.group("seconds")));
      //dateText.replace(mtch.group(), "");
    }

    return date;
  }

  @Override
  public LocalDateTime deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException
  {
    LocalDateTime ldt;

    try
    {
      ldt = LocalDateTime.parse(jp.getValueAsString());
    }
    catch(DateTimeParseException dtpe)
    {
      // TODO: maybe something else here?

      ldt = fromDelimited(jp.getValueAsString());
    }

    return ldt;
  }
}
